import isEmpty from 'lodash/isEmpty'

export default function validateInput(data){
	let errors = {};

	const fieldRequiredNumber = 'Set number, plese!';
    
    data.bidValue = parseInt(data.bidValue)
	if(!data.bidValue || !Number.isInteger(data.bidValue)){
		errors.bidValue = fieldRequiredNumber
    }else if(data.lastBid && data.lastBid >= data.bidValue){
        errors.bidValue = "Value must be higher than previous bid"
    }else if(!data.lastBid && data.bidValue <= 0){
        errors.bidValue = "Value must be greater than 0"
    }

	return {
		errors,
		isValid: isEmpty(errors)
	}

}