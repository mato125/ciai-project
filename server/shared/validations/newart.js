import Validator from  'validator'
import isEmpty from 'lodash/isEmpty'

export default function validateInput(data){
    let errors = {};
    
	const fieldRequired = 'This field is required';
    const filedDate = 'Set valid date, please';
    const filedIsNotUrl = 'Please set valid url address';
    
	if(Validator.isNull(data.name)){
		errors.name = fieldRequired
	}

	if(Validator.isNull(data.description)){
		errors.description = fieldRequired
    }
    
    if(Validator.isNull(data.techniques)){
		errors.techniques = fieldRequired
    }

    if(Validator.isNull(data.imgUrl)){
		errors.imgUrl = fieldRequired
    }else if(!Validator.isURL(data.imgUrl)){
        errors.imgUrl = filedIsNotUrl
    }

    if(Validator.isNull(data.created) || Validator.isAfter(data.created) ){
		errors.created = filedDate
    }
    
    if(isEmpty(data.tags)){
        errors.tags = fieldRequired
    }
    
    console.log(errors)
	return {
		errors,
		isValid: isEmpty(errors)
	}

}