import { autorun, observable, inject, action } from 'mobx'
import axios from 'axios'
import fakeData from '../data'

class GalleryStore{
    @observable content = [];
    @observable authors = [];
    @observable bids = [];
    

    constructor() {
        this.getGalleryFromDb()
        this.getAuthorsFromDb()
        this.getBidsFromDb()
    }


    @action
    getGalleryFromDb(){

        const t = this;
        axios({
            method: 'GET',
            url: 'http://localhost:8080'+'/arts'
        })
        .then(function (response) {
           t.content = response.data;

        })      
        .catch(function (error) {
            t.content = []
        });
        
    }

    @action
    getAuthorsFromDb(){

            const t = this;
            axios({
                method: 'GET',
                url: 'http://localhost:8080'+'/authors'
            })
            .then(function (response) {
                t.authors = response.data;
            })      
            .catch(function (error) {
                t.authors = []
            });
        
    }

    @action
    getBidsFromDb(){
        const t = this;
            axios({
                method: 'GET',
                url: 'http://localhost:8080'+'/bids'
            })
            .then(function (response) {
                let bids = [];
                for(let b of response.data){
                    bids.push(JSON.parse(b))
                }
                t.bids = bids;

            })      
            .catch(function (error) {
                t.bids = []
            });
    }

    @action
    addNewArt(data){
        const t = this;
        return new Promise(function(resolve, reject) {
            axios({
                method: 'POST',
                url: 'http://localhost:8080/auth/newart',
                data: data
            })
            .then(function (response) {
                t.getGalleryFromDb();
                resolve(response);
            })
            .catch(function (error) {
                reject(error.response.data);
            });
        })
    }
    
    @action
    getArtById(id){
        return this.content.find(art => art.id == id)
    }

    @action
    getAuthorById(id){
        return this.authors.find(author => author.id == id)
    }

    @action
    getBidById(id){
        return this.bids.find(bid => bid.id == id)
    }

    @action
    changeAvailability(value, artId){
        const t = this;
        axios({
            method: 'POST',
            url: 'http://localhost:8080/auth/art/'+artId+'/?availability=' + value,
            data: {}
        })
        .then(function (response) {
            t.getArtById(artId).available = value;
        })    
    }

    @action
    getAuthorsGallery(id){
        let gallery = [];
        for(var art of this.content){
            if(art.author.id == id){
                gallery.push(art)
            }
        }
        return gallery;
    }

    @action
    getGalleryByKeywordId(name){
        let galleryWithKeyword = [];
        for(var art of this.content){
            for(var keyWord of art.keywords){
                if(keyWord.word == name){
                    galleryWithKeyword.push(art)
                }
            }

        }
        return galleryWithKeyword;
    }

    @action
    getBidsOfArt(id){
        return this.getArtById(id).bids;
    }

    @action
    getActualBid(id){
        const bids = this.getBidsOfArt(parseInt(id));
        if(bids.length > 0){
            const lastBid = bids[bids.length - 1];
            return lastBid
        }
        return null
    }
    @action
    makeBid(data){
                
        const t = this;

        return axios({
            method: 'POST',
            url: 'http://localhost:8080/auth/makeBid',
            data: data
        })
    
    }

    @action
    acceptBid(bidId){
        const t = this;
        axios({
            method: 'POST',
            url: 'http://localhost:8080/auth/bid/'+bidId+'/?action=' + 'accept',
            data: {}
        })
        .then(function (response) {
            t.getBidsFromDb()
            t.getGalleryFromDb()
        });
    }
    @action
    rejectBid(bidId){
        const t = this;
        axios({
            method: 'POST',
            url: 'http://localhost:8080/auth/bid/'+bidId+'/?action=' + 'reject',
            data: {}
        })
        .then(function (response) {
            t.getBidsFromDb()
            t.getGalleryFromDb()
        });

    }
    @action
    makePublic(bidId){
        const t = this;
        axios({
            method: 'POST',
            url: 'http://localhost:8080/auth/bid/'+bidId+'/publish',
            data: {}
        })
        .then(function (response) {
            t.getBidsFromDb()
            t.getGalleryFromDb()
        });
    }
    @action
    getUsersBids(userId){
        let bids = []
        for(let b of this.bids){
            if(b.userId == userId){
                bids.push(b)
            }
        }
        return bids
    }
    

}

export default new GalleryStore
