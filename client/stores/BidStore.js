import { autorun, observable } from 'mobx'

class BidStore{
	@observable content = new Set()
	@observable filter = '';

	addToBid(id){
		this.content = new Set(this.content.add(id));
	}
}

export default new BidStore
