import { autorun, observable } from 'mobx'
import axios from 'axios'
import jwt from 'jsonwebtoken'
import fakeData from '../data'

import PATH_TO_SERVER from '../index.js'


class UserStore{
	@observable data = {}
	@observable isLogged = false

	logIn(userData){

		const t = this;

		return new Promise(function(resolve, reject) {
			axios({
				method: 'POST',
				url: 'http://localhost:8080/login',
				data: userData
			})
			.then(function (response) {
				const token = response.data.token;
				t.setAuthorizationToken(token)
				t.setCurrentUser(jwt.decode(token))
				resolve(response);
			})
			.catch(function (error) {
				reject(error);
			});
		})

	}

	setAuthorizationToken(token){
		if(token){
			localStorage.setItem('jwtToken', token)
			axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
		}else{
			delete axios.defaults.headers.common['Authorization'];
		}
	}

	setCurrentUser(userData){
		this.data = userData;
		this.isLogged = true;
	}

	logOut(){
		localStorage.removeItem('jwtToken');
		setAuthorizationToken(false)
		this.isLogged = false;
		this.data = {};
	}

	registerNewUser(userData){
		
		return new Promise(function(resolve, reject) {
			axios({
				method: 'POST',
				url: 'http://localhost:8080/register',
				data: userData
			})
			.then(function (response) {
				resolve(response);
			})
			.catch(function (error) {
				reject(error);
			});
		});
	}
}

export default new UserStore
