import { autorun, observable } from 'mobx'

class FlashMessageStore{
	@observable content = {}

	addFlashMessage(data, id = new Date().getUTCMilliseconds()){
		//whole object must be replaced

		let messages = {}
		//copy old
		for (var property in this.content) {
		    if (this.content.hasOwnProperty(property)) {
		        messages[property] = this.content[property]
		    }
		}

		//add new
		messages[id] = data;
		this.content = messages;
	}
	deleteFlashMessage(id){
		//whole object must be replaced
		let messages = {}
		for (var property in this.content) {
		    if (this.content.hasOwnProperty(property) && property != id) {
		        messages[property] = this.content[property]
		    }
		}
		this.content = messages;
	}

	emptyMessage(){
		this.content = {}
	}
}

export default new FlashMessageStore
