import React from 'react'
import { render } from 'react-dom'
import styles from './css/style.less'
import jwt from 'jsonwebtoken'
import { Router, browserHistory } from 'react-router'
import routes from './routes'

import { Provider } from 'mobx-react'
import BidStore from './stores/BidStore.js'
import UserStore from './stores/UserStore.js'
import GalleryStore from './stores/GalleryStore.js'
import FlashMessageStore from './stores/FlashMessageStore.js'

export const PATH_TO_SERVER = 'http://localhost:8080';

const stores  = {
	bid: BidStore,
	user: UserStore,
	flashMessage: FlashMessageStore,
	gallery: GalleryStore
}

if(localStorage.jwtToken){
	stores.user.setAuthorizationToken(localStorage.jwtToken)
	stores.user.setCurrentUser(jwt.decode(localStorage.jwtToken))
}

render(
	<Provider stores={stores}>
		<Router history={browserHistory} routes={routes} />
	</Provider>
	, 
	document.getElementById('app') 
);