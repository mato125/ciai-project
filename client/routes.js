import React from 'react'
import { Route, IndexRoute, hashHistory, Router } from 'react-router'

import Layout from './components/Layout'
import Art from "./components/gallery/Art"
import Gallery from "./components/gallery/Gallery"
import Home from "./components/Home"
import Author from "./components/author/Author"
import Authors from "./components/author/Authors"
import SignUp from "./components/signup/SignUp"
import Login from "./components/login/Login"
import Bids from "./components/bids/Bids"
import NewArt from "./components/newArt/NewArt"

export default (
	<Route path='/' component={Layout} >
		<IndexRoute component={Home} />
		<Route path='arts(/:art)' component={Art} />
		<Route path='gallery(/:keyword)' component={Home} />
		<Route path='authors' component={Authors} />
		<Route path='signup' component={SignUp} />
		<Route path='login' component={Login} />
		<Route path='newart' component={NewArt} />
		<Route path='bids' component={Bids} />
		<Route path='authors(/:author)' component={Author} />
	</Route>
);
