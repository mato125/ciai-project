import Validator from  'validator'
import isEmpty from 'lodash/isEmpty'

export default function validateInput(data){
	let errors = {};

	const fieldRequired = 'This field is required';
	const filedIsNotUrl = 'Please set valid url address';

	if(Validator.isNull(data.username)){
		errors.username = fieldRequired
	}

	if(Validator.isNull(data.firstName)){
		errors.firstName = fieldRequired
	}

	if(Validator.isNull(data.lastName)){
		errors.lastName = fieldRequired
	}

	if(Validator.isNull(data.password)){
		errors.password = fieldRequired
	}

	if(Validator.isNull(data.description)){
		errors.description = fieldRequired
	}
	
	if(Validator.isNull(data.imgUrl)){
		   errors.imgUrl = fieldRequired
	   }else if(!Validator.isURL(data.imgUrl)){
		   errors.imgUrl = filedIsNotUrl
	   }

	if(Validator.isNull(data.passwordConf)){
		errors.passwordConf = fieldRequired
	}

	if(!Validator.equals(data.password, data.passwordConf)){
		errors.passwordConf = 'Password must match'
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}

}