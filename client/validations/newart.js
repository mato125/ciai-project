import Validator from  'validator'
import isEmpty from 'lodash/isEmpty'

export default function validateInput(data){
    let errors = {};
    
	const fieldRequired = 'This field is required';
    const filedDate = 'Set valid date, please';
    const filedIsNotUrl = 'Please set valid url address';
    const contentIsEmpty = 'Please set at least one img of the art';
    
	if(Validator.isNull(data.name)){
		errors.name = fieldRequired
	}

	if(Validator.isNull(data.description)){
		errors.description = fieldRequired
    }
    
    if(Validator.isNull(data.techniques)){
		errors.techniques = fieldRequired
    }

    if(data.content.length == 0){
        errors.content = contentIsEmpty
    }else{
        for(let i = 0; i < data.content.length; i++){
            if(!Validator.isURL(data.content[i].text)){
                errors.content = filedIsNotUrl
            }
        }
    }


    if(isEmpty(data.tags)){
        errors.tags = fieldRequired
    }
    
	return {
		errors,
		isValid: isEmpty(errors)
	}

}