import Validator from  'validator'
import isEmpty from 'lodash/isEmpty'

export default function validateInput(data){
	let errors = {};

	const fieldRequired = 'This field is required';

	if(Validator.isNull(data.username)){
		errors.username = fieldRequired
	}

	if(Validator.isNull(data.password)){
		errors.password = fieldRequired
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}

}