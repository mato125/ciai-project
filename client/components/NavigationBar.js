import React from 'react'
import { Link } from 'react-router'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class NavigationBar extends React.Component{
	
	logOut(){
		this.props.stores.flashMessage.emptyMessage()
		let message = { type: 'success', text: 'Goodbye '+this.props.stores.user.data.firstName +' !' }
		this.props.stores.flashMessage.addFlashMessage(message, 'logOut')
		this.props.stores.user.logOut();
	}

	render(){
			return(
				<nav className="navbar navbar-inverse">
					<div className="container-fluid">
						<div className="navbar-header">
							<Link to='/gallery' className='navbar-brand'>
								<img id='nav-logo' src='https://upload.wikimedia.org/wikipedia/commons/a/ab/Logo_TV_2015.png' alt='logo'/>
							</Link>
						</div>
						<ul className="nav navbar-nav">
							<li><Link to='/gallery'>Gallery</Link></li>
							<li><Link to='/authors'>Authors</Link></li>
						</ul>
						{this.props.stores.user.isLogged ? (
							<ul className="nav navbar-nav navbar-right">
								<li>
									<Link to="bids">
										<span>Bids </span>
										<span className='notification'>{this.props.stores.bid.content.size}</span>
									</Link>
								</li>
								<li>
									<Link to={{ pathname: 'authors/'+this.props.stores.user.data.id }}>
										<span className="glyphicon glyphicon-user"></span> 
										My profile
									</Link>
								</li>
								<li>
									<Link to='#' onClick={this.logOut.bind(this)}>
										<span className="glyphicon glyphicon-log-out"></span> 
										Logout
									</Link>
								</li>
							</ul>
						) : (
							<ul className="nav navbar-nav navbar-right">
								<li>
									<Link to='signup'><span className="glyphicon glyphicon-user"></span> Sign Up</Link>
								</li>
								<li>
									<Link to='login'><span className="glyphicon glyphicon-log-in"></span> Login</Link>
								</li>
							</ul>
						)}
					</div>
				</nav>
		);
	}	
}

