import React from 'react'
import { Link } from 'react-router'
import map from 'lodash/map'
import classnames from 'classnames'
import { observer } from 'mobx-react'
import BidRecived from './BidRecived'
import BidSent from './BidSent'

@observer(['stores'])
export default class Bids extends React.Component{

    constructor(){
        super()

        this.acceptBid = this.acceptBid.bind(this)
        this.rejectBid = this.rejectBid.bind(this)

    }
    acceptBid(bidId){ 
        this.props.stores.gallery.acceptBid(bidId)
    }

    rejectBid(bidId){
        this.props.stores.gallery.rejectBid(bidId)
    }

    makePublic(bidId){
        this.stores.gallery.makePublic(bidId)
    }

	render(){
        const stores = this.props.stores;
        const userData = stores.user.data
        stores.gallery.content
        const usersGallery = stores.gallery.getAuthorsGallery(userData.id)
        const userBids= stores.gallery.getUsersBids(userData.id)
		return(
            <div>
                {usersGallery &&  userBids ? (
                <div className='row'>
                    <div className='col-xs-6'>
                        <h2>Bids of my art</h2>
                        <table className="table table-bordered text-center">
                            <tbody>
                                {usersGallery.map((art) =>
                                    <BidRecived 
                                        key={art.id}
                                        artData={art}
                                        rejectBid={this.rejectBid}
                                        acceptBid={this.acceptBid}
                                    /> 
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div className='col-xs-6'>
                        <h2>My bids</h2>
                        <table className="table table-bordered text-center">
                            <tHead>
                                <tr>
                                    <td>name of art</td>
                                    <td>value</td>
                                    <td>status</td>
                                    <td></td>
                                </tr>
                            </tHead>
                            <tbody>
                                {userBids.map((bid) =>
                                    <BidSent 
                                        key={bid.id}
                                        bidData={bid}
                                        makePublic={this.makePublic}
                                    /> 
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
                ):(
                    <p>waiting on data</p>
                )}
            </div>
        );
    }
}