import React from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'
import { observer } from 'mobx-react'
import BuyButton from '../gallery/BuyButton'

@observer(['stores'])
export default class BidSent extends React.Component{

    constructor(props){
        super(props)

        this.makePublic = this.makePublic.bind(this)
    }

    makePublic(e, bidId){
        e.preventDefault()
        this.props.makePublic(bidId)
    }
    
	render(){
        const bidData = this.props.bidData;
        const artData = this.props.stores.gallery.getArtById(bidData.artId);
        return(
            <tr>
               <td className="text-left">
                    {artData.name}
               </td>
               <td>
                    {bidData.value}
               </td>
               <td>
                    {bidData.status}
               </td>
                <td>
                    {bidData.status == "accepted" &&
                        <form className='inline' onSubmit={(e) => this.makePublic(e, bidData.id)}>
                            <button  type='submit' className='btn btn-success'>
                                publish
                            </button>
                        </form>
                    }
               </td>
            </tr>
        );
    }
} 
