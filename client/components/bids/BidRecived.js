import React from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'
import map from 'lodash/map'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class BidRecived extends React.Component{

    constructor(props){
        super(props)
        
        this.accept = this.accept.bind(this)
        this.reject = this.reject.bind(this)
    }

    accept(e, bidId){
        e.preventDefault()
        this.props.acceptBid(bidId)
    }

    reject(e, bidId){
        e.preventDefault()
        this.props.rejectBid(bidId)
    }

    
	render(){
        const stores = this.props.stores;
        const artData = this.props.artData;

        let bids = [];
        for(let i = 0; i < artData.bids.length; i++){
            let bidInfo = stores.gallery.getBidById(artData.bids[i].id);
            let classname = classnames({ "text-warning": bidInfo.status == 'rejected', "text-success": bidInfo.status == 'accepted'})
            bids.push(
                <tr key={i}>
                    <td>{i+1}.</td>
                    <td>
                        {bidInfo.value} &euro;
                    </td>
                    <td>
                        {stores.gallery.getAuthorById(bidInfo.userId).username}
                    </td>
                    <td className='text-center width-600'>
                        {(bidInfo.status == 'new') ? (
                            <div>
                                <form className='inline' onSubmit={(e) => this.accept(e, bidInfo.id)}>
                                    <button  type='submit' className='btn btn-success'>
                                        accept
                                    </button>
                                </form>
                                <form className='inline' onSubmit={(e) => this.reject(e, bidInfo.id)}>
                                    <button  type='submit' className='btn btn-danger'>
                                        reject
                                    </button>
                                </form>
                            </div>
                        ):(
                            <span className={classname}>{bidInfo.status}</span>
                        )
                        }
                    </td>
                </tr>
            )
            
        }
        
        return(
            <tr>
                <td className='text-cente'>
                    <div className='bidRecived-gallery-wrapper'>
                        <Link to={{ pathname: 'arts/' + artData.id }}>
                            <h5>{artData.name}</h5>
                            <img alt='art photo' src={artData.content[0].url} />
                        </Link>
                    </div>
                </td>
                <td>
                    <table className="table table-bordered author-bids-requests">
                        <thead>
                            <tr>
                            <td><strong>#</strong></td>
                            <td><strong>amout</strong></td>
                            <td><strong>user</strong></td>
                            <td><strong>status</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            {bids}
                        </tbody>
                    </table>
                </td>
            </tr>
          
        );
    }
} 
