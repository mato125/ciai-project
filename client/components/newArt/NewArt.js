import React from 'react'
import data from  '../../data'
import NewArtForm from './NewArtForm'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class NewArt extends React.Component{
    static contextTypes = {
	  router: React.PropTypes.object.isRequired
	}

    componentWillMount(){
        if(!this.props.stores.user.isLogged){
            this.props.stores.flashMessage.addFlashMessage({text: 'Please login', type: 'warning'})
            this.context.router.push('/login');
        } 
    }

    render(){
        return(
            <div className='row'>
                <div className='col-xs-12 col-md-6 col-md-offset-3'>
                    <NewArtForm />
                </div>
            </div>
        )
    }
}
