import React from 'react'
import data from  '../../data'
import validateInput from '../../validations/newart'
import TextFieldGroup from '../common/TextFieldGroup'
import NumberFieldGroup from '../common/NumberFieldGroup'
import TextAreaGroup from '../common/TextAreaGroup'
import RadioButtonsInput from '../common/RadioButtonsInput'
import TagsInput from '../common/TagsInput'
import { WithContext as ReactTags } from 'react-tag-input';
import { withRouter } from 'react-router';
import { observer } from 'mobx-react'

@observer(['stores'])
export default class NewArtForm extends React.Component{

    constructor(props){
		super(props)
		this.state = {
			name: 'nameee',
            description: 'descripiptiqwe',
            techniques: 'technoicicicaca',
            content: [{id: new Date().getMilliseconds(), text: 'http://www.scalarchives.com/web/img/slideshow/02.jpg'}],
			errors: {},
			price: 1000,
            available: true,
            tags: [{id: new Date().getMilliseconds(), text: 'haha'}],
			isLoading: false,
			ivnalid: false
		}
		
		this.radioButtonValues = {
			'Yes': true,
			'No': false,
		}
		this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }
 
	onChange(e){
		if(e.target.name == 'available'){
			const val = e.target.value == 'true' ? true : false;
			this.setState({
				[e.target.name]: val
			})
		}else if(e.target.name == 'price'){
			this.setState({
				[e.target.name]: parseInt(e.target.value)
			})
		}else{
			this.setState({
				[e.target.name]: e.target.value
			})
		}
	}

	isValid(){
		const { errors, isValid } = validateInput(this.state)
        if(!isValid){
			this.setState({ errors })
		}
		return isValid;
	}

	onSubmit(e){
        e.preventDefault()
        const t = this;
		if(this.isValid()){
            this.setState( {errors: {}, isLoading: true });
            this.props.stores.gallery.addNewArt(this.state)
            .then(function(response){
            	let message = { type: 'success', text: 'New art was successfully added to your gallery' }
                t.props.stores.flashMessage.addFlashMessage(message)
                t.context.router.push('/authors/'+t.props.stores.user.data.id);
            })
            .catch(function(error){
            	t.setState( { errors: error, isLoading: true });
				t.setState( { isLoading: false });
            })
		}
    }   
    
    render(){
        const errors = this.state.errors;
        const { tags } = this.state;
		return(
			<form onSubmit={this.onSubmit}>
				<h1 className='text-center'>New art!</h1>
				<TextFieldGroup
					error={errors.name}
					label="name"
					onChange={this.onChange}
					value={this.state.name}
					field="name"
				/>

				<TextAreaGroup
					error={errors.description}
					label="description"
					onChange={this.onChange}
					value={this.state.description}
					field="description"
				/>

				<TagsInput 
                    error={errors.content}
                    field='content'
                    label="content"
                    tags={this.state.content}
                    placeholder={"You can add url by hitting enter"}
                />

                <TextFieldGroup
					error={errors.techniques}
					label="techniques"
					onChange={this.onChange}
					value={this.state.techniques}
					field="techniques"
				/>

                <TagsInput 
                    error={errors.tags}
                    field='tags'
                    label="keywords"
                    tags={this.state.tags}
                    placeholder={"You can add keyword by hitting enter"}
                />
				
				<NumberFieldGroup
					error={errors.price}
					label="price"
					onChange={this.onChange}
					value={this.state.price}
					field="price"
					type="number"
					step="100"
				/>

				<RadioButtonsInput
					error={errors.available}
					field='available'
					label='for sale'
					onChange={this.onChange}
					options={this.radioButtonValues}
					value={this.state.available}
				/>

				<div className='form-group'>
					<button type='submit' disabled={this.state.isLoading || this.state.invalid} className='btn btn-primary btn-lg'>
						Add
					</button>
				</div>
			</form>
		)
    }

    static contextTypes = {
        router: React.PropTypes.object.isRequired
    }

}