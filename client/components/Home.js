import React from 'react'
import { Link } from 'react-router'
import Gallery from './gallery/Gallery'
import { withRouter } from 'react-router';
import { observer } from 'mobx-react'

@observer(['stores'])
export default class Home extends React.Component{
	render(){

		let gallery = null;
		let title = null;
		if(this.props.params.keyword){
			gallery = this.props.stores.gallery.getGalleryByKeywordId(this.props.params.keyword)
			title = "keyword: " + this.props.params.keyword
		}else{
			gallery = this.props.stores.gallery.content;
			title = "Gallery of artists"
		}
		return(
			<div>
				<h1>{title}</h1>
				<Gallery gallery={gallery} />
			</div>
		)
	}
}