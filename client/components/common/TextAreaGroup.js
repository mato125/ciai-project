import React from 'react'
import classnames from 'classnames'

const TextAreaGroup = ({ field, value, label, error, onChange, checkUserExists, placeholder, id }) =>{
	return(
		<div className={classnames("form-group", { "has-error": error})}>
			<label className='control-label'>{label}</label>
			<textarea
				onChange={onChange}
				value={value}
				name={field}
				id={id}
				className='form-control'
                placeholder={placeholder}
                rows='6'
			/>
			{error && <span className='help-block'>{error}</span>}
		</div>
	)
}

TextAreaGroup.propTypes = {
	field: React.PropTypes.string.isRequired,
	value: React.PropTypes.string.isRequired,
	label: React.PropTypes.string.isRequired,
	error: React.PropTypes.string,
	onChange: React.PropTypes.func.isRequired
}

export default TextAreaGroup