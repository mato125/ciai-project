import React from 'react'
import classnames from 'classnames'


export default class NumberFieldGroup extends React.Component{

	render(){

		const { field, value, label, error, type, step, onChange, placeholder, id, disabled, min } = this.props;

		return(
			<div className={classnames("form-group", { "has-error": error})}>
				<label className='control-label'>{label}</label>
				<input
					onChange={onChange}
					value={value}
					name={field}
					id={id}
					className='form-control'
					placeholder={placeholder}
					type={type}
					step={step}
					disabled={!disabled}
					min={min}
				/>
				{error && <span className='help-block'>{error}</span>}
			</div>
		)
	}

}

