import React from 'react'
import { WithContext as ReactTags } from 'react-tag-input';
import classnames from 'classnames'

export default class TagsInput extends React.Component{
    constructor(){
        super()
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
    }

    handleDelete(i) {
        let tags = this.props.tags;
        tags.splice(i, 1);
    }
 
    handleAddition(tag) {
        let tags = this.props.tags;
        tags.push({
            id: new Date().getMilliseconds(),
            text: tag
        });
    }
 
    handleDrag(tag, currPos, newPos) {
        let tags = this.props.tags;
 
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);
    }

    render(){
        const { field, label, error, placeholder, tags } = this.props;
        
        return(
            <div className={classnames("form-group", { "has-error": error})}>
                <label className='control-label'>{label}</label>
                <ReactTags 
                    tags={tags}
                    handleDelete={this.handleDelete}
                    handleAddition={this.handleAddition}
                    handleDrag={this.handleDrag} 
                    placeholder={placeholder}
                    classNames={{
                        tags: 'tagsClass',
                        tagInput: 'tag-input',
                        tagInputField: 'form-control',
                        selected: 'selectedClass',
                        tag: 'tag label label-primary label-lg tag-label',
                        remove: 'tag-delete',
                    }}
                />
                {error && <span className='help-block'>{error}</span>}
            </div>
        );
    }
}

TagsInput.propTypes = {
	field: React.PropTypes.string.isRequired,
	tags: React.PropTypes.array.isRequired,
	label: React.PropTypes.string.isRequired,
	error: React.PropTypes.string
}
