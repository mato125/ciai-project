import React from 'react'
import classnames from 'classnames'
import map from 'lodash/map'

export default class RadioButtonsInput extends React.Component{
    render(){
        const { field, label, error, value, onChange, options } = this.props
        const buttons = map(options, (val, key) =>
            <span className="radio" key={key}>
                <label className={classnames("btn btn-md  btn-primary", { "active ": val == value})}>
                <input type="radio" className='invisible' value={val}  checked={val == value} name={field} onChange={onChange}/>
                    {key}
                </label>
            </span>    
        )
        return(
            <div className={classnames("form-group", { "has-error": error})}>
                <label>{label}</label>
                <div>
                    {buttons}
                </div>
                {error && <span className='help-block'>{error}</span>}
            </div>
        )
    }
}

RadioButtonsInput.propTypes = {
	field: React.PropTypes.string.isRequired,
	value: React.PropTypes.bool.isRequired,
	label: React.PropTypes.string.isRequired,
	onChange: React.PropTypes.func.isRequired,
	error: React.PropTypes.string,
}
