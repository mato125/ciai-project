import React from 'react'
import classnames from 'classnames'
import data from  '../../data'
import map from 'lodash/map'
import validateInput from '../../validations/login'
import TextFieldGroup from '../common/TextFieldGroup'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class LoginForm extends React.Component{
	

	constructor(props){
		super(props)
		this.state = {
			username: '',
			password: '',
			errors: {},
			isLoading: false,
			ivnalid: false
		}
		this.onChange = this.onChange.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
	}

	onChange(e){
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	isValid(){
		const { errors, isValid } = validateInput(this.state)
		if(!isValid){
			this.setState({ errors })
		}

		return isValid;
	}

	onSubmit(e){
		e.preventDefault()
		if(this.isValid()){
			this.setState( {errors: {}, isLoading: true });
			const t = this;
			this.props.stores.user.logIn(this.state)
				.then(function (response) {
					t.props.stores.flashMessage.emptyMessage()
					let userdata = t.props.stores.user.data;
					let message = { type: 'success', text: 'Wellcome '+ userdata.username +' !' }
					t.props.stores.flashMessage.addFlashMessage(message, 'logged')
					t.setState({ isLoading: false})
					t.context.router.push('/authors/'+userdata.id);
				})
				.catch(function (error) {
					t.setState({ errors: error.response.data.errors, isLoading: false})
				});			
			
			/*
			if(!logged){
				let message = { type: 'error', text: 'Invalid credentials.' }
				this.props.stores.flashMessage.addFlashMessage(message, 'logginError')
			}else{
				this.props.stores.flashMessage.emptyMessage()
				let userdata = this.props.stores.user.data;
				let message = { type: 'success', text: 'Wellcome '+ userdata.firstName +' !' }
				this.props.stores.flashMessage.addFlashMessage(message, 'logged')
				this.context.router.push('/authors/'+userdata.id);

			}
			this.setState( { isLoading: false });*/
		}else{
			
		}
	}
	render(){
		const errors = this.state.errors;
		return(
			<form onSubmit={this.onSubmit}>
				<h1 className='text-center'>Login!</h1>
				{ errors.form && <div className='alert alert-danger'>{errors.form}</div> }
				<TextFieldGroup
					error={errors.username}
					label="username"
					onChange={this.onChange}
					value={this.state.username}
					field="username"
				/>

				<TextFieldGroup
					error={errors.password}
					label="password"
					onChange={this.onChange}
					value={this.state.password}
					field="password"
					type='password'
				/>

				<div className='form-group'>
					<button type='submit' disabled={this.state.isLoading || this.state.invalid} className='btn btn-primary btn-lg'>
						Login
					</button>
				</div>
			</form>
		)
	}

	static contextTypes = {
	  router: React.PropTypes.object.isRequired
	}
}
