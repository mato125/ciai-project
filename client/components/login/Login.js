import React from 'react'
import classnames from 'classnames'
import LoginForm  from './LoginForm'

export default class Login extends React.Component{
	render(){
		return(
			<div className='row'>
				<div className='col-md-4 col-xs-12 col-md-offset-4'>
					<LoginForm />
				</div>
			</div>
		)
	}
}