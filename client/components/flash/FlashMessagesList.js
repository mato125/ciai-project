import React from 'react'
import FlashMessage from './FlashMessage'
import map from 'lodash/map'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class FlashMessagesList extends React.Component{

	render(){
		const messages = map(this.props.stores.flashMessage.content, (val, key) =>
			<FlashMessage 	key={key} 
							text={val.text} 
							type={val.type} 
							id={key} 
							flashMessageStore={this.props.stores.flashMessage}
			/>
		);

		return(
			<div>{messages}</div>
		)
	}
}
