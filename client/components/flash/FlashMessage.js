import React from 'react'
import classnames from 'classnames'

export default class FlashMessage extends React.Component{
	constructor(props){
		super(props)
		this.onClick = this.onClick.bind(this);
	}

	onClick(){
		this.props.flashMessageStore.deleteFlashMessage(this.props.id)
	}
	render(){
		const { id, type, text } = this.props;
		return(
			<div className={classnames('alert', {
				'alert-success': type === 'success',
				'alert-danger': type === 'error',
				'alert-warning': type === 'warning'
			})}>
			<button onClick={this.onClick} className='close'>&times;<span></span></button>
			{text}
			</div>
		)
	}
}

FlashMessage.propTypes = {
	id: React.PropTypes.string.isRequired,
	text: React.PropTypes.string.isRequired,
	type: React.PropTypes.string.isRequired,
	flashMessageStore: React.PropTypes.object.isRequired
}