import React from 'react'
import data from  '../../data'
import map from 'lodash/map'
import classnames from 'classnames'
import { Link } from 'react-router'
import Moment from 'moment';
import BuyButton from './BuyButton'
import ArtKeywords from './ArtKeywords'
import RadioButtonsInput  from '../common/RadioButtonsInput'
import Gallery   from './Gallery'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class Art extends React.Component{

    constructor(props){
        super(props)
    }



     render(){
        
        Moment.locale('en');
        const art = this.props.stores.gallery.getArtById(this.props.params.art)
        let content;
        if(art){
            const author = this.props.stores.gallery.getAuthorById(art.author.id)
            if(author){
                content =   
                    <div className='row'>
                        <div className='col-xs-12 col-md-6 text-center'>
                            <img className='art-image' src={art.content[0].url} />
                        </div>
                        <div className='col-xs-12 col-md-6'>
                            <h3>
                                {art.name}<br/>
                            </h3>
                            <p>{art.description}</p>
                            <p>author:&nbsp;
                                <Link to={{ pathname: 'authors/' + art.author.id }}>
                                    {art.author.firstName} {art.author.lastName}
                                </Link>
                            </p>
                            <p>created: {Moment(art.created).format('d. MMM YYYY')}</p>
                            <p>techniques: {art.techniques}</p>
                            <p>keywords: 
                                 <ArtKeywords keywords={art.keywords} />
                            </p>
                            {art.price && <p>price: {art.price} &euro; </p>}
                            <BuyButton 
                                available={art.available} 
                                id={art.id} 
                                price={art.price}
                                authorId={art.author.id}
                                bids={art.bids}
                            />
                        </div>
                        <div className='col-xs-12'>
                            <h2>Author's gallery</h2>
                            <Gallery gallery={this.props.stores.gallery.getAuthorsGallery(art.author.id)}/>
                        </div>
                    </div>
            }else{
                content = <p>waiting for data...</p>
            }
        }else{
            content = <p>waiting for data...</p>
        }
        return(
            <div>{content}</div>
        );
    }
}
