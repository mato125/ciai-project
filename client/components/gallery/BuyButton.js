import React from 'react'
import classnames from 'classnames'
import NumberFieldGroup from '../common/NumberFieldGroup'
import RadioButtonsInput  from '../common/RadioButtonsInput'
import validateInput from '../../validations/bids'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class Author extends React.Component{

    constructor(props){
        super(props);


        const { available, bids }  = this.props;
        this.state = this.getValues(available, bids)
        this.submitBid = this.submitBid.bind(this);
        this.onChange = this.onChange.bind(this);
		this.availabilityChange = this.availabilityChange.bind(this)
        
        this.radioButtonValues = {
            'Yes': true,
            'No': false,
		}
		
    }

    availabilityChange(e){
        const val = e.target.value == 'true' ? true : false;
        this.props.stores.gallery.changeAvailability(val, this.props.id )
    }

    getValues(available, bids){
        
        const actualBid = bids[bids.length - 1];

        let actualValueLabel = null;
        let minValue = null;

        if (available) {
            if(actualBid){
                actualValueLabel = 'actual bid: '+ actualBid.value + " €";
                minValue = Math.ceil((actualBid.value + actualBid.value*0.1)/100)*100;
            }else if(this.props.price){
                actualValueLabel = 'no bids yet'
                minValue = this.props.price;
            }else{
                actualValueLabel = 'no price set'
                minValue = 100;
            }
        }else{
            actualValueLabel = "not available"
            minValue = 0;
        }

        return {
            bidValue: minValue,
            lastBid: actualBid ? actualBid.value : null,
            actualValueLabel: actualValueLabel,
            available: this.props.available,
			errors: {},
			isLoading: false,
			invalid: false
        }
    }

	onChange(e){
		this.setState({
			[e.target.name]: e.target.value
        })
	}

	isValid(){
		const { errors, isValid } = validateInput(this.state)
		if(!isValid){
			this.setState({ errors })
		}

		return isValid;
	}
    
	submitBid(e){
        e.preventDefault()
        const t = this;
        if(this.isValid()){
            let data = {
                value: Math.ceil(this.state.bidValue/100)*100,
                status: "new",
                artId: this.props.id
            }

            this.props.stores.gallery.makeBid(data)
            .then(function(response){
                t.props.stores.gallery.getArtById(t.props.id).bid = response.data;
                t.setState(t.getValues(t.props.available, response.data))
            })
            .catch(function(error){
            })
        }
    }

    render(){
        
        let content = null;
        if(!this.props.stores.user.isLogged){
            content = <h4>{this.state.actualValueLabel}</h4>
        }else if (this.props.stores.user.data.id == this.props.authorId) {
            content = 
                <RadioButtonsInput
                    field='available'
                    label='for sale'
                    onChange={this.availabilityChange}
                    options={this.radioButtonValues}
                    value={this.props.available}
                />
        }else{
            content =        
            <form onSubmit={this.submitBid}>
                <div className='form-group'>
                    <label>{this.state.actualValueLabel}</label>
                    <div className="input-group">
                        <NumberFieldGroup
                            field="bidValue"
                            value={this.state.bidValue}
                            error={this.state.errors.bidValue}
                            type="number"
                            step="100"
                            min={this.state.minValue}
                            placeholder="Set value"
                            onChange={this.onChange}
                            disabled={this.props.available}
                        />
                        <span className="input-group-btn verticalalign-top">
                            <input type="submit" className="btn btn-primary" disabled={!this.props.available} value="Make a bid!" />
                        </span>
                    </div>
                </div>
            </form>
        }

        return (
            <div>
                {content}
            </div>
        );
    }    
}
