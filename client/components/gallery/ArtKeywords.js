import React from 'react'
import { Link } from 'react-router'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class ArtKeywords extends React.Component{

    render(){
        const links = this.props.keywords.map((val, key) =>
            <Link key={val.id} to={{ pathname: 'gallery/' + val.word }} className='cursor-pointer ' key={val.id}>
                #{val.word}
            </Link>
        )
        return(
            <span>
                {links}
            </span>
        )
    }
}