import React from 'react'
import { Link } from 'react-router'
import data from  '../../data'
import map from 'lodash/map'
import classnames from 'classnames'
import BuyButton from './BuyButton'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class Gallery extends React.Component{

	render(){
		return(
            <div className='row'>
				{this.props.gallery.map((art) =>

					<div key={art.id} className='col-xs-3 gallery'>	
						<div className="thumbnail">
							<Link to={{ pathname: 'arts/'+art.id }}>
								<div className='galler-image-wrapper'>
									<img src={art.content[0] ? art.content[0].url : ""} alt="title image" />
								</div>
								<div className="caption">
									<p>{art.name}<br/>
									</p>
								</div>
							</Link>

							<div className='footer'>
								<BuyButton 
									available={art.available} 
									id={art.id} 
									price={art.price}
									authorId={art.author.id}
									bids={art.bids}
								/>
								
								<Link to={{ pathname: 'authors/' + art.author.id }}>
									<small className='thumb-author-name'>
										{art.author.firstName} {art.author.lastName}
									</small>
								</Link>
							</div>
							
						</div>
					</div>
				)}
			</div>
        );
    }
}