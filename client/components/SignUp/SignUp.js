import React from 'react'
import classnames from 'classnames'
import SignUpForm  from './SignUpForm'

export default class SignUp extends React.Component{
	render(){
		return(
			<div className='row'>
				<div className='col-md-4 col-xs-12 col-md-offset-4'>
					<SignUpForm />
				</div>
			</div>
		)
	}
}