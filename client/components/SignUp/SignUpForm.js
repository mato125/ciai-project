import React from 'react'
import classnames from 'classnames'
import data from  '../../data'
import map from 'lodash/map'
import validateInput from '../../validations/signup'
import TextFieldGroup from '../common/TextFieldGroup'
import TextAreaGroup from '../common/TextAreaGroup'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class SignUpForm extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			username: '',
			firstName: '',
			lastName: '',
			password: '',
			passwordConf: '',
			description: '',
			imgUrl: '',
			errors: {},
			isLoading: false
		}
		this.onChange = this.onChange.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
	}
	onChange(e){
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	isValid(){
		const { errors, isValid } = validateInput(this.state)
		if(!isValid){
			this.setState({ errors })
		}

		return isValid;
	}

	onSubmit(e){
		e.preventDefault()
		this.setState( {errors: {}, isLoading: true });
		if(this.isValid()){
			const t = this;
			this.props.stores.user.registerNewUser(this.state)
				.then(function (response) {
					let message = { type: 'success', text: 'Registration successfull, you can log in now !' }
					t.props.stores.flashMessage.addFlashMessage(message)
					t.setState( { isLoading: false });
					t.context.router.push('/login');
				})
				.catch(function (error) {
					t.setState({ errors: error.response.data, isLoading: false})
				});			
		}else{
			this.setState( { isLoading: false });
		}
	}
	render(){
		const errors = this.state.errors;
		return(
			<form onSubmit={this.onSubmit}>
				<h1 className='text-center'>Join our community!</h1>

				<TextFieldGroup
					error={errors.username}
					label="username"
					onChange={this.onChange}
					value={this.state.username}
					field="username"
				/>

				<TextFieldGroup
					error={errors.firstName}
					label="first name"
					onChange={this.onChange}
					value={this.state.firstName}
					field="firstName"
				/>

				<TextFieldGroup
					error={errors.lastName}
					label="last name"
					onChange={this.onChange}
					value={this.state.lastName}
					field="lastName"
				/>

				<TextFieldGroup
					error={errors.imgUrl}
					label="image url"
					onChange={this.onChange}
					value={this.state.imgUrl}
					field="imgUrl"
				/>

				<TextAreaGroup
					error={errors.description}
					label="description"
					onChange={this.onChange}
					value={this.state.description}
					field="description"
				/>

				<TextFieldGroup
					error={errors.password}
					label="password"
					onChange={this.onChange}
					value={this.state.password}
					field="password"
					type="password"
				/>

				<TextFieldGroup
					error={errors.passwordConf}
					label="passwordConf"
					onChange={this.onChange}
					value={this.state.passwordConf}
					field="passwordConf"
					type="password"
				/>
				
				<div className='form-group'>
					<button type='submit' disabled={this.state.isLoading} className='btn btn-primary btn-lg'>
						Sign up
					</button>
				</div>
			</form>
		)
	}

	static contextTypes = {
		router: React.PropTypes.object.isRequired
	  }
}