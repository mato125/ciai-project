import React from 'react'
import { Link } from 'react-router'
import map from 'lodash/map'
import classnames from 'classnames'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class Authors extends React.Component{


	render(){
        const authors = map(this.props.stores.gallery.authors, (author) =>
            <div key={author.id} className='col-xs-3'>			
                <div className="thumbnail">
                    <Link to={{ pathname: 'authors/'+author.id }}>
                        <div className='photo-wrapper'>
                            <img src={author.imgUrl} alt="author image" />
                        </div>
                        <div className="caption text-center">
                            <h5>{author.firstName} {author.lastName}
                            </h5>
                        </div>
                    </Link>                
                </div>
            </div>
        );

		return(
            <div className='row author-gallery'>
                {authors}
            </div>
        );
    }
}