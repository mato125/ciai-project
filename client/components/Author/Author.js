import React from 'react'
import data from  '../../data'
import classnames from 'classnames'
import Moment from 'moment';
import Gallery from '../gallery/Gallery'
import { Link } from 'react-router'
import { observer } from 'mobx-react'

@observer(['stores'])
export default class Author extends React.Component{
        
    render(){
        const author = this.props.stores.gallery.getAuthorById(this.props.params.author)
        const userStore = this.props.stores.user;
        const userData = this.props.stores.gallery.getAuthorById(userStore.data.id) 
        return(
            <div>
                {author &&
                <div>
                    <div className='row'>
                        <div className='col-xs-12 col-md-6 text-center author-photo'>
                            <img src={author.imgUrl} />
                        </div>
                        <div className='col-xs-12 col-md-6'>
                            <h3>
                                <small>{author.firstName} {author.lastName}</small>
                            </h3>
                            <p>
                                {author.description}
                            </p>
                            <p>
                                {(userStore && userStore.isLogged  && userData.id == author.id) && 
                                    <Link to='newArt' className='btn btn-success btn-lg'>Add new art</Link>
                                }
                            </p>
                        </div>
                    </div>

                    <div className='row mat'>
                        {(userStore && userStore.isLogged  && userData.id == author.id) ?(
                            <h4 className='text-left'> My gallery </h4>
                        ) : (
                            <h4 className='text-left'> Author's gallery</h4>
                        )}
                        <Gallery gallery={this.props.stores.gallery.getAuthorsGallery(author.id)}/>
                    </div>
                </div>
                }
            </div>
        );
    }
}