export default {
	gallery: {
		1:{
				id: 1,
				name: 'Lorem ipsum',
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis. Vivamus vehicula velit non metus faucibus auctor. Nam sed augue. Donec orci. Cras eget diam et dolor dapibus sollicitudin. In lacinia, tellus vitae laoreet ultrices, lectus ligula dictum dui, eget condimentum velit dui vitae ante. Nulla nonummy augue nec pede. Pellentesque ut nulla.",
				imgUrl:"https://thumb9.shutterstock.com/display_pic_with_logo/510316/583771252/stock-photo-the-poster-with-a-portrait-of-a-beautiful-girl-in-the-style-of-contemporary-art-583771252.jpg",
				author: 1,
				techniques: "co aj viem jake techniky",
				created: "2017-04-27 22:06:17.355+02",
				keywords : [3],
				available: true,
				price: 2200,
				bids: [1, 2 , 3 , 4]
		},
		2:{
				id: 2,
				name: 'Lorem ipsum 2',
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis.",
				imgUrl:"https://thumb7.shutterstock.com/display_pic_with_logo/228151/459092947/stock-photo-abstract-art-creative-background-hand-painted-background-459092947.jpg",
				author: 1,
				techniques: "ine techniky"   ,
				created: "2013-04-27 22:06:17.355+02",
				keywords : [0, 1 ,2, 3],
				available: false,
				price: 23400,
				bids: [3 ]
		},
		3:{
				id: 3,
				name: 'Lorem ipsum 3',
				description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc.",
				imgUrl:"https://thumb1.shutterstock.com/display_pic_with_logo/1098617/110746526/stock-photo-yellow-and-grey-abstract-art-painting-110746526.jpg",
				author: 2,
				techniques: "jebanie stetcom po platne" ,
				created: "2014-04-17 22:06:17.355+02",
				keywords : [2 , 3],
				available: true,
				price: 3000,
				bids: [4, 5]
		},
		4:{
				id: 4,
				name: 'Lorem ipsum 4',
				description: " Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc.",
				imgUrl:"https://az616578.vo.msecnd.net/files/2017/03/08/636245430126517293426674257_art-ph.jpg",
				author: 3,
				techniques: "jebanie stetcom po platne" ,
				created: "2014-04-17 22:06:17.355+02",
				keywords : [2 , 1],
				available: true,
				price: 1200,
				bids: []
		}
	},
	authors: {
			1:{
					id: 1,
					userName: 'Bmojsej',
					password: '123',
					firstName: "Brano",
					lastName: "Mojsej",
					imgUrl: "https://thumb1.shutterstock.com/display_pic_with_logo/2117717/445337599/stock-photo-artist-creative-designer-illustrator-graphic-skill-concept-445337599.jpg",
					description:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis."
			},
			2:{
					id: 2,
					userName: 'Jprocko',
					password: '123',
					firstName: "Jozo",
					lastName: "Procko",
					imgUrl: "https://thumb9.shutterstock.com/display_pic_with_logo/334153/487267645/stock-photo-the-artist-draws-graffiti-487267645.jpg",
					description:"Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis."
			},
			3:{
				id: 3,
				userName: 'RoboF',
				password: '2',
				firstName: "Robo",
				lastName: "Fico",
				imgUrl: "https://t4.aimg.sk/magaziny/DGQZrByiQzWknIuRwdHYWA~Robo-fico-sexy-robert-slovak.jpg?t=LzEyMDB4NjMw&h=qrRS7NJ88trUh52-kZy8zQ&e=2145916800&v=2",
				description:"Wrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl. Suspendisse lacinia, odio non feugiat vestibulum, sem erat blandit metus, ac nonummy magna odio pharetra felis."
			}
	},
	keywords: {
			0: "Sculcupture",
			1: "Painting",
			2: "Music",
			3: "Sadness"
	},
	bids:{
		1:{
			id: 1,
			userId: 2,
			artId: 1,
			value: 6200,
			status: 'overbidden'
		},
		2:{
			id: 2,
			userId: 3,
			artId: 1,
			value: 6400,
			status: 'overbidden'
		},
		3:{
			id: 3,
			userId: 2,
			artId: 1,
			value: 6500,
			status: 'overbidden'
		},
		4:{
			id: 4,
			userId: 3,
			artId: 1,
			value: 6600,
			status: 'accepted'
		},
		5:{
			id: 5,
			userId: 1,
			artId: 3,
			value: 4700,
			status: 'new'
		}
	}

}

