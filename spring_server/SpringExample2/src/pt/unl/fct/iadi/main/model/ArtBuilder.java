package pt.unl.fct.iadi.main.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class ArtBuilder {

    private Art art;

    public ArtBuilder() {
        art = new Art();
    }

    public ArtBuilder id(int id) {
        art.setId(id);
        return this;
    }

    public ArtBuilder name(String name) {
        art.setName(name);
        return this;
    }

    public ArtBuilder author(User author) {
        art.setAuthor(author);
        return this;
    }

    public ArtBuilder owner(User owner) {
        art.setOwner(owner);
        return this;
    }

    public ArtBuilder description(String description) {
        art.setDescription(description);
        return this;
    }

    public ArtBuilder publicOwner(Boolean publicOwner) {
        art.setPublicOwner(publicOwner);
        return this;
    }

    public ArtBuilder available(Boolean available) {
        art.setAvailable(available);
        return this;
    }

    public ArtBuilder price(Integer price) {
        art.setPrice(price);
        return this;
    }

    public ArtBuilder technique(String technique) {
        art.setTechnique(technique);
        return this;
    }

    public ArtBuilder created(Date created) {
        art.setCreated(created);
        return this;
    }
    
    public ArtBuilder content(List<Content>  content) {
    	art.setContent(content);
    	return this;
    }
    
    public ArtBuilder keywords(List<Keyword>  keywords) {
    	art.setKeywords(keywords);
    	return this;
    }

    public Art build() {
        return art;
    }
}