package pt.unl.fct.iadi.main.model;

        import com.fasterxml.jackson.annotation.JsonBackReference;

        import javax.persistence.*;

@Entity(name = "content")
public class Content {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="content_id_seq")
	@SequenceGenerator(name="content_id_seq", sequenceName="content_id_seq", allocationSize=1)
	@Column(name = "id")
    int id;
    String url;
    String type;
    
    @ManyToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="art_id")
    @JsonBackReference
    private Art art;

    public Content() {}

    public Content(String url, String type, Art art) {
        this.url = url;
        this.type = type;
        this.art = art;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String toString() {
        return id + ":" +  ", " + type ;
    }

    public Art getArt(){
        return art;
    }
    public void setArt(Art art){
        this.art=art;
    }

}
