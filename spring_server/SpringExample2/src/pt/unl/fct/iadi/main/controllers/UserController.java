package pt.unl.fct.iadi.main.controllers;



import org.json.JSONObject;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pt.unl.fct.iadi.main.exceptions.RegistrationValidaitonFailedException;
import pt.unl.fct.iadi.main.exceptions.ResourceNotFoundException;
import pt.unl.fct.iadi.main.model.Art;
import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserRepository;
import pt.unl.fct.iadi.main.dto.JwtData;
import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.dto.UserForLogin;
import pt.unl.fct.iadi.main.dto.UserForRegistration;
import pt.unl.fct.iadi.main.services.UserServiceImpl;

@RestController
public class UserController {
 
	@Autowired
	UserServiceImpl usersService;
	
	@Autowired
    UserRepository userRepository;
		
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public ResponseEntity loginUser(@RequestBody JwtData jwtUser) {
		try {
			try {
				String token = usersService.loginUser(jwtUser.getUsername(), jwtUser.getPassword());
				String jsonResponse = new JSONObject()
				        .put("token", token)
				        .toString();
				
				return ResponseEntity
			            .status(HttpStatus.OK)                 
			            .body(jsonResponse);
				
			} catch(ResourceNotFoundException e) {
				String jsonResponse = new JSONObject()
				        .put("errors", new JSONObject().put("form", "Invalid Credentials"))			        
				        .toString();
				
				return ResponseEntity
			            .status(HttpStatus.UNAUTHORIZED)                 
			            .body(jsonResponse);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
		            .status(HttpStatus.INTERNAL_SERVER_ERROR)                 
		            .body("JSON ERROR");
		} 

    }
	
	@RequestMapping(value="/register", method = RequestMethod.POST)
	public ResponseEntity registerUser(@RequestBody UserForRegistration newUser) {

		try {
			
			usersService.registerNewUser(newUser);
			
			return ResponseEntity
		            .status(HttpStatus.OK)                 
		            .body("");
			
		} catch(RegistrationValidaitonFailedException e) {
			String jsonResponse = e.getErrors().toJson().toString();
			
			return ResponseEntity
		            .status(HttpStatus.BAD_REQUEST)                 
		            .body(jsonResponse);
		} 
	}
	
	@RequestMapping(value="/authors", method= RequestMethod.GET)
    ResponseEntity getAll(@RequestParam(required=false, value="") String search) {
		
    	Iterable<User> artsFromDb = userRepository.findAll();
			
		return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(artsFromDb);		
    }
	
	//napr. http://localhost:8080/auth/first/4/tasks/3?hovno=va1&hovno2=val2
	@RequestMapping(value="/auth/first/{id}/tasks/{id2}", method = RequestMethod.POST)
	public void first(@RequestBody UserForLogin userData, @PathVariable(value="id") int id, @PathVariable(value="id2") int id2,
			 @RequestParam(value="hovno") String hovnoVal,
             @RequestParam(value="hovono2") String hovnoVal2
			) {
		System.out.println(id);
		System.out.println(id2);
		System.out.println(hovnoVal);
		System.out.println(hovnoVal2);
		System.out.println(userData.getUsername());
		System.out.println(userData.getPassword());
		System.out.println(userData.getKeywords().toString());
		System.out.println(userData.getObj().toString());
		
	}

	
	@RequestMapping(value="/auth/do", method = RequestMethod.GET)
	public ResponseEntity needsToBeLogged(@RequestHeader(value="Authorization", required=false, defaultValue="") String jwt, Authentication auth) {
		JwtUserDetails loggedUserDetails = ((JwtUserDetails)auth.getPrincipal());
		System.out.println(loggedUserDetails.getId());
		try {
			
			String jsonResponse = new JSONObject()
			        .put("qadasd", "QWE")
			        .toString();

				return ResponseEntity
			            .status(HttpStatus.OK)                 
			            .body(jsonResponse);

		} catch (JSONException e) {
			e.printStackTrace();
			return ResponseEntity
		            .status(HttpStatus.INTERNAL_SERVER_ERROR)                 
		            .body("JSON ERROR");
		} 

    }

}