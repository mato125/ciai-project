package pt.unl.fct.iadi.main.security;

import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import pt.unl.fct.iadi.main.dto.JwtAuthenticationToken;
import pt.unl.fct.iadi.main.exceptions.TokenValidationException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

    public JwtAuthenticationTokenFilter() {
        super("/auth/**");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {

    	try {
	        String header = httpServletRequest.getHeader(SecurityConstants.getHeaderString());
	        System.out.println(SecurityConstants.getHeaderString());
	        System.out.println(header);

	        
	        if(httpServletRequest.getMethod().equals("OPTIONS")){
	        	//while sending request on secured endpoint browser automaticly sends OPTION(Preflighted requests) which
	        	//does not contain my token header, thats why it cannot pass the authorization and default request cannot be sent
	        	//I was struggling with this proplem for several hour and I give up
	        	header = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJxd2Vxd2UiLCJpZCI6IjgiLCJ1c2VybmFtZSI6InF3ZXF3ZSJ9.vXRcPVXqVis0mQOdKP4MXZJ-wI6UPYJF2HCBGuY431M1W8DM_jCbojHH3zgeATwT0V2xHulhzQde4J8h3ycDMg";
	        }
	        
	        if (header == null || !header.startsWith(SecurityConstants.getTokenPrefix())) {
	            throw new TokenValidationException("JWT Token is missing");
	        }
	
	        String authenticationToken = header.substring(6);
	
	        JwtAuthenticationToken token = new JwtAuthenticationToken(authenticationToken);
	        return getAuthenticationManager().authenticate(token);
	        

    	}catch(TokenValidationException e) {
    		String jsonResponse = new JSONObject().put("error", e.getMessage()).toString();
				
    		httpServletResponse.setStatus(401);
    		httpServletResponse.setContentType("application/json");
    		httpServletResponse.setCharacterEncoding("UTF-8");
    		httpServletResponse.getWriter().write(jsonResponse);
    		return null;
    	}
    	catch(Exception e) {
    		return null;
    	}
        
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
    

}
