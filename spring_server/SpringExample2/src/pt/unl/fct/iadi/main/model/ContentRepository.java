package pt.unl.fct.iadi.main.model;

        import org.springframework.data.jpa.repository.Query;
        import org.springframework.data.repository.CrudRepository;


public interface ContentRepository extends CrudRepository<Content,Integer> {
    Content findById(Integer id);

    @Query(value = "select c.id as id, c.url as url, c.art_id as art_id, c.type as type from Content c JOIN Art a ON a.id=c.art_id where a.id = ?1",nativeQuery = true)
    Content[] findByTheArtIdNumber(Integer id);
}
