package pt.unl.fct.iadi.main.dto;

import java.util.List;
import java.util.Set;
import java.sql.Date;
import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import pt.unl.fct.iadi.main.model.User;


public class ArtToBeAdded {

    String name;
    String description;
	String techniques;
	
	List<HashMap<String, String>> content;
	List<HashMap<String, String>> tags;

	int author;
	
	boolean available;
	
    int price;
    
    public ArtToBeAdded() {};

    public String getName() {
		return name;
	}    

    public String getDescription() {
		return description;
	}
    
    public void setTags(List<HashMap<String, String>> tags) {
		this.tags = tags;
	}

	public String getTechniques() {
		return techniques;
	}  
    
    
	public List<HashMap<String, String>> getContent() {
		return content;
	}

	public void setContent(List<HashMap<String, String>> content) {
		this.content = content;
	}

	public List<HashMap<String, String>> getTags() {
		return tags;
	}

	public void setKeywords(List<HashMap<String, String>> tags) {
		this.tags = tags;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public int getAuthor() {
		return author;
	}    
    
    public void setAuthor(int author) {
		this.author = author;
	}
	
    
    public void setName(String name) {
		this.name = name;
	}    

    public void setDescription(String description) {
    	this.description = description;
	}

    public void setTechniques(String techniques) {
    	this.techniques = techniques;
	} 

    public void setPrice(int price) {
    	this.price = price;
	}  
    
    public int getPrice() {
    	return price;
	}        
    
}
