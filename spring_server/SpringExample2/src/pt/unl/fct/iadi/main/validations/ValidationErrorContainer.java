package pt.unl.fct.iadi.main.validations;

import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;


public class ValidationErrorContainer {

	private HashMap<String, String> errors;

	public ValidationErrorContainer() {
		this.errors = new HashMap<String, String>();
	}
	
	void put(String key, String message) {
		this.errors.put(key, message);
	}
	
	public HashMap<String, String> getErrors(){
		return this.errors;
	}
		
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		try {
			for (HashMap.Entry<String, String> entry : this.errors.entrySet()){
					json.put(entry.getKey(), entry.getValue());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
}
