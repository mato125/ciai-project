package pt.unl.fct.iadi.main.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import pt.unl.fct.iadi.main.dto.JwtAuthenticationToken;
import pt.unl.fct.iadi.main.dto.JwtData;
import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.exceptions.TokenValidationException;

import java.util.List;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private JwtValidator jwtValidator;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {

        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) usernamePasswordAuthenticationToken;
        String token = jwtAuthenticationToken.getToken();

        JwtData userDataFromToken = jwtValidator.validate(token);

        if (userDataFromToken == null) {
            throw new TokenValidationException("JWT Token is incorrect");
        }

        List<GrantedAuthority> authorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("user");
        
        JwtUserDetails jwtUserDetails = new JwtUserDetails();
        
        jwtUserDetails.setAuthorities(authorities);
        jwtUserDetails.setToken(token);
        jwtUserDetails.setId( userDataFromToken.getId());
        jwtUserDetails.setUserName(userDataFromToken.getUsername());
        
        return jwtUserDetails;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return (JwtAuthenticationToken.class.isAssignableFrom(aClass));
    }
}
