package pt.unl.fct.iadi.main.model;

import java.sql.Date;

public class BidBuilder {

    private Bid bid;

    public BidBuilder() {
        bid = new Bid();
    }

    public BidBuilder id(int id) {
        bid.setId(id);
        return this;
    }

    public BidBuilder user(User user) {
        bid.setUser(user);
        return this;
    }

    public BidBuilder art(Art art) {
        bid.setArt(art);
        return this;
    }

    public BidBuilder value(Integer value) {
        bid.setValue(value);
        return this;
    }

    public BidBuilder status(String status) {
        bid.setStatus(status);
        return this;
    }

    public BidBuilder seen_by_bidder(Boolean seen_by_bidder) {
        bid.setSeen_by_bidder(seen_by_bidder);
        return this;
    }

    public BidBuilder seen_by_owner(Boolean seen_by_owner) {
        bid.setSeen_by_owner(seen_by_owner);
        return this;
    }
    
    public Bid build() {
        return bid;
    }
}
