package pt.unl.fct.iadi.main.controllers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.model.Bid;
import pt.unl.fct.iadi.main.model.BidRepository;
import pt.unl.fct.iadi.main.dto.ArtToBeAdded;
import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.dto.NewBidData;
import pt.unl.fct.iadi.main.exceptions.BadRequestException;
import pt.unl.fct.iadi.main.model.Art;
        import pt.unl.fct.iadi.main.model.ContentRepository;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserRepository;
import pt.unl.fct.iadi.main.services.ArtServiceImpl;
import pt.unl.fct.iadi.main.services.BidServiceImpl;


// Inspired in: https://spring.io/guides/gs/rest-service/

@RestController
public class BidController {

    @Autowired
    BidRepository bidRepo;
    
    @Autowired
    UserRepository userRepo;
    
    @Autowired
    ArtRepository artRepo;
    
    @Autowired
    BidServiceImpl bidService;
    
    
    
    @RequestMapping(value="/bids", method= RequestMethod.GET)
    ResponseEntity getAll() {
		
    	/*
    	Bid[] bidsFromDb = bidRepo.findAllWithUsers();
    	
    	NOT WORKING
    	
		return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(bidsFromDb);		
	    */
    	
    	Bid[] bidsFromDb = bidRepo.findAllWithUsers();
    	
    	List<String> result = new ArrayList<String>();
    	
    	System.out.println(bidsFromDb.length);
    	for (Bid bid : bidsFromDb) {
    		
    		JSONObject row = new JSONObject();
    		row.put("id", bid.getId());
    		row.put("artId",bid.getArt().getId());
    		row.put("seenByBidder",bid.getSeen_by_bidder());
    		row.put("seenByOwner",bid.getSeen_by_owner());
    		row.put("status",bid.getStatus());
    		row.put("userId",bid.getUser().getId());
    		row.put("value",bid.getValue());
    		
    		result.add(row.toString());
    		
    	}
    	
    	return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(result);
    }
    

    @RequestMapping(value="/bids/{id}", method = RequestMethod.GET)
    ResponseEntity  showBid(@PathVariable Integer id) {
    	
    	Bid bidsFromDb = bidRepo.findById(id);
		
		return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(bidsFromDb);		

    }
    
    @RequestMapping(value="/auth/makeBid", method = RequestMethod.POST)
    ResponseEntity  makeBid( @RequestBody NewBidData newbid, Authentication auth) {
    	
    	JwtUserDetails loggedUserDetails = ((JwtUserDetails)auth.getPrincipal());
    	User loggedUser = userRepo.findById(loggedUserDetails.getId());
    	try {
    		bidService.makeNewBid(loggedUser, newbid);
    		return ResponseEntity
    	            .status(HttpStatus.OK)                 
    	            .body(bidRepo.findByArtId(newbid.getArtId()));
    	}catch(BadRequestException e) {
    		return ResponseEntity
    	            .status(HttpStatus.BAD_REQUEST)                 
    	            .body(e.getMessage());
    	}
    }
     
    @RequestMapping(value="/auth/bid/{bidId}", method = RequestMethod.POST)
    @PreAuthorize("@securityService.canModifyBid(principal, #bidId)")
    public ResponseEntity changeBidStatus(
    		@PathVariable(value="bidId", required=true) int bidId, 
    		@RequestParam(value="action", required=true) String aciton,
    		Authentication auth)
    {
    	Bid bid = bidRepo.findById(bidId);
    	if(bid != null) {
    		if(aciton.equals("accept")) {   			
    			bidService.acceptBid(bid);
    			Art art = artRepo.findById(bid.getArt().getId());
    			art.setAuthor(bid.getUser());
    			artRepo.save(art);
    		}else if(aciton.equals("reject")) {
    			bid.setStatus("rejected");
    	    	bidRepo.save(bid);
    		}
	    	return ResponseEntity
		            .status(HttpStatus.OK)                 
		            .body("");
    	}else {
    		return ResponseEntity
    	            .status(HttpStatus.BAD_REQUEST)                 
    	            .body("No bid");
    	}
    	
    	
    }
    
    @RequestMapping(value="/auth/bid/{bidId}/publish", method = RequestMethod.POST)
    @PreAuthorize("@securityService.canPublishBid(principal, #bidId)")
    public ResponseEntity setBidPublished(
    		@PathVariable(value="bidId", required=true) int bidId)
    {
    	Bid bid = bidRepo.findById(bidId);
    	if(bid != null) {
			bid.getArt().setPublicOwner(true);
			bid.setStatus("published");
			bidRepo.save(bid);
			
			return ResponseEntity
		            .status(HttpStatus.OK)                 
		            .body("");	    	
    	}else {
    		return ResponseEntity
    	            .status(HttpStatus.BAD_REQUEST)                 
    	            .body("No bid");
    	}
    	
    	
    }
}

