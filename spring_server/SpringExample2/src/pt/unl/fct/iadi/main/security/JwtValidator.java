package pt.unl.fct.iadi.main.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import pt.unl.fct.iadi.main.dto.JwtData;
import pt.unl.fct.iadi.main.exceptions.TokenValidationException;
import pt.unl.fct.iadi.main.model.User;

import org.springframework.stereotype.Component;

@Component
public class JwtValidator {

    public JwtData validate(String token) {
    	JwtData user = null;        
    	try {
            Claims body = Jwts.parser()
                    .setSigningKey(SecurityConstants.getSecret())
                    .parseClaimsJws(token)
                    .getBody();

            
            user = new JwtData();
            user.setUsername(((String)body.getSubject()));
            user.setId(Integer.parseInt((String) body.get("id")));
        }
        catch (Exception e) {
            throw new TokenValidationException("Invalid token data");
        }

        return user;
    }
}
