package pt.unl.fct.iadi.main.model;

import java.util.List;

public class KeywordBuilder {

    private Keyword keyword;

    public KeywordBuilder() {
        keyword = new Keyword();
    }

    public KeywordBuilder id(int id) {
        keyword.setId(id);
        return this;
    }

    public KeywordBuilder word(String word) {
        keyword.setWord(word);
        return this;
    }

    public Keyword build() {
        return keyword;
    }
    
    public KeywordBuilder arts(List<Art> arts) {
    	keyword.setArts(arts);
    	return this;
    }
}
