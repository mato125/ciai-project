package pt.unl.fct.iadi.main.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

import java.util.List;
import java.util.Set;

@Entity(name = "keyword")
@Table(name = "keywords")
public class Keyword {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="keywords_id_seq")
	@SequenceGenerator(name="keywords_id_seq", sequenceName="keywords_id_seq", allocationSize=1)
	@Column(name = "id")
    int id;

    String word;
    
    @ManyToMany(mappedBy="keywords", fetch = FetchType.LAZY)
    @JsonBackReference
    private List<Art> arts;

    public Keyword() {}

    public Keyword(Integer id, String word) {
        this.id = id;
        this.word = word;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Art> getArts() {
		return arts;
	}

	public void setArts(List<Art> arts) {
		this.arts = arts;
	}

	public void setWord(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }
}
