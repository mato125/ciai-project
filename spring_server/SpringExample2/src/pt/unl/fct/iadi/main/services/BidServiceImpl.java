package pt.unl.fct.iadi.main.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.unl.fct.iadi.main.dto.NewBidData;
import pt.unl.fct.iadi.main.exceptions.BadRequestException;
import pt.unl.fct.iadi.main.model.Art;
import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.model.Bid;
import pt.unl.fct.iadi.main.model.BidBuilder;
import pt.unl.fct.iadi.main.model.BidRepository;
import pt.unl.fct.iadi.main.model.User;

@Service
public class BidServiceImpl {

	@Autowired
	BidRepository bidRepo;
		
	@Autowired
	ArtRepository artRepo;
	
	public BidServiceImpl() {}
	
	public void makeNewBid(User user, NewBidData bidData) {
		Art art = artRepo.findById(bidData.getArtId());
		if(art != null) {
			Bid lastBid = bidRepo.getLastBidOfArt(bidData.getArtId());
			
			if(lastBid == null || (lastBid != null && lastBid.getValue() < bidData.getValue())) {
				BidBuilder bidBuilder = new BidBuilder()
				.art(art)
				.user(user)
				.seen_by_bidder(false)
				.seen_by_owner(false)
				.status("new")
				.value(bidData.getValue());
				
				bidRepo.save(bidBuilder.build());
			}else{
				throw new BadRequestException("New value of bid can not be lower than last bid");
			}
		}else {
			throw new BadRequestException("There is not such art");
		}
		
	}

	public void acceptBid(Bid bid) {
		List<Bid> allBidsOfArt = bidRepo.findByArtId(bid.getArt().getId());
		for(Bid bidOfArt : allBidsOfArt) {
			if(bidOfArt.getId() != bid.getId()) {
				bidOfArt.setStatus("rejected");
			}
			bidRepo.save(bidOfArt);
		}
    	
    	bid.setStatus("accepted");
    	bidRepo.save(bid);
		
	}
	
}
