package pt.unl.fct.iadi.main.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User,Integer> {
    User findById(Integer id);
    
    @Query(value = "SELECT "
    					+ "u.id,"
    					+ "u.username,"
		    			+ "u.first_name,"
		    			+ "u.last_name,"
		    			+ "u.hashed_salted_password,"
		    			+ "u.description,"
		    			+ "u.img_url "
	    			+ "FROM users AS u "
	    				+ "WHERE u.username = ?",nativeQuery = true)
    User findByUsername(String username);
}

