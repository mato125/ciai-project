package pt.unl.fct.iadi.main.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.security.Key;

@Entity(name = "art_keywords_map")
@Table(name = "art_keywords_map")
public class Art_keywords_map {

    @Id
    @GeneratedValue
    int id;

    @ManyToOne
    @JoinColumn(name="art_id")
    @JsonManagedReference
    private Art art;

    @ManyToOne
    @JoinColumn(name="keyword_id")
    @JsonBackReference
    private Keyword keyword;

    public Art_keywords_map() {}

    public Art_keywords_map(Integer id, Keyword keyword, Art art) {
        this.id = id;
        this.art=art;
        this.keyword=keyword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Keyword getKeyword(){
        return keyword;
    }
    public void setKeyword(Keyword keyword){
        this.keyword=keyword;
    }

    public Art getArt(){
        return art;
    }
    public void setArt(Art art){
        this.art=art;
    }
    
}

