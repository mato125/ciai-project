package pt.unl.fct.iadi.main.model;

import java.util.List;

public class ContentBuilder {

    private Content content;

    public ContentBuilder() {
        content = new Content();
    }

    public ContentBuilder id(int id) {
        content.setId(id);
        return this;
    }
    

    public ContentBuilder art(Art art) {
        content.setArt(art);
        return this;
    }

    public ContentBuilder url(String url) {
        content.setUrl(url);
        return this;
    }

    public ContentBuilder type(String type) {
        content.setType(type);
        return this;
    }

    public Content build() {
        return content;
    }
}
