package pt.unl.fct.iadi.main.controllers;

        import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.bind.annotation.RestController;

        import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.dto.ArtToBeAdded;
import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.dto.UserForRegistration;
import pt.unl.fct.iadi.main.exceptions.BadRequestException;
import pt.unl.fct.iadi.main.exceptions.NewArtValidaitonFailedException;
import pt.unl.fct.iadi.main.exceptions.RegistrationValidaitonFailedException;
import pt.unl.fct.iadi.main.exceptions.ResourceNotFoundException;
import pt.unl.fct.iadi.main.model.Art;
        import pt.unl.fct.iadi.main.model.ContentRepository;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserRepository;
import pt.unl.fct.iadi.main.security.SecurityService;
import pt.unl.fct.iadi.main.services.ArtServiceImpl;


// Inspired in: https://spring.io/guides/gs/rest-service/

@RestController
public class ArtsController {

    @Autowired
    ArtRepository artRepo;
    
    @Autowired
    UserRepository userRepo;
    
    ContentRepository content;

	@Autowired
	ArtServiceImpl artsService;
	
	@Autowired
	SecurityService securityService;
    
	
    @RequestMapping(value="/arts", method= RequestMethod.GET)
    ResponseEntity getAll(@RequestParam(required=false, value="") String search) {
		
    	Iterable<Art> artsFromDb = artRepo.findAll();
    	artsFromDb.forEach(Art::galleryData);
		return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(artsFromDb);		
    }
    

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    ResponseEntity  showArt(@PathVariable Integer id) {
    	
    	Art artsFromDb = artRepo.findById(id);
		
		return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(artsFromDb);		

    }
    
    @RequestMapping(value="/auth/art/{artId}", method = RequestMethod.POST)
    @PreAuthorize("@securityService.canModifyArt(principal, #artId)")
    public ResponseEntity changeAvailability(@PathVariable(value="artId", required=true) int artId, @RequestParam(value="availability", required=true) Boolean availability,
    		Authentication auth) {
    	
    	Art art = artRepo.findById(artId);
    	art.setAvailable(availability);
    	artRepo.save(art);
    	
    	return ResponseEntity
	            .status(HttpStatus.OK)                 
	            .body(art);
    	
    }
    
    @RequestMapping(value="/auth/newart", method = RequestMethod.POST)
    public ResponseEntity newArt(@RequestBody ArtToBeAdded newArt, Authentication auth) {
		try {
			System.out.println("TO SOM");
			JwtUserDetails loggedUserDetails = ((JwtUserDetails)auth.getPrincipal());
	    	User loggedUser = userRepo.findById(loggedUserDetails.getId());
	    	try {
	    		artsService.addNewArt(newArt, loggedUser);
	    		System.out.println("TO SOM OK");
	    		return ResponseEntity
	    	            .status(HttpStatus.OK)                 
	    	            .body(artRepo.findAll());
	    		
	    	}catch(NewArtValidaitonFailedException e) {
				String jsonResponse = e.getErrors().toJson().toString();
				System.out.println("TO SOM NOOK");
				return ResponseEntity
			            .status(HttpStatus.BAD_REQUEST)                 
			            .body(jsonResponse);
			} 
			
		}catch(BadRequestException e) {
			
			return ResponseEntity
		            .status(HttpStatus.BAD_REQUEST)                 
		            .body(e.getMessage());
		}
    		
    }

    
}

