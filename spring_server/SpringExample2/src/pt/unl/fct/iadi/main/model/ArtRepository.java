package pt.unl.fct.iadi.main.model;

import org.hibernate.annotations.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface ArtRepository extends CrudRepository<Art,Integer> {
    Art findById(Integer id);

    @Query(value = "select a.id as id, a.technique as technique, a.author, a.name from Art a where a.id = ?1",nativeQuery = true)
    Art[] findByTheIdNumber(Integer id);
    
    @Query(value = "select * from Art a where a.name = ?1",nativeQuery = true)
    Art findByName(String name);
    
    @Query(value = "select * from Art a where a.id = ?1 AND author = ?2",nativeQuery = true)
	Object artBelongsToUser(int artId, int userId);

}

