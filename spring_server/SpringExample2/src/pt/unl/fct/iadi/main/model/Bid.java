package pt.unl.fct.iadi.main.model;

        import com.fasterxml.jackson.annotation.JsonBackReference;
        import com.fasterxml.jackson.annotation.JsonManagedReference;
        import org.hibernate.annotations.Table;

        import javax.persistence.*;
        import java.util.Date;
        import java.util.Set;

// Basic JPA configuration: https://spring.io/guides/gs/accessing-data-jpa/
// For mysql configuration: https://spring.io/guides/gs/accessing-data-mysql/

@Entity(name = "bid")
public class Bid {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="bid_id_seq")
	@SequenceGenerator(name="bid_id_seq", sequenceName="bid_id_seq", allocationSize=1)
	@Column(name = "id")
    int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonBackReference
    User user;

    @ManyToOne(fetch = FetchType.LAZY) 
    @JoinColumn(name = "art_id", nullable = false)
    @JsonBackReference
    Art art;

    Integer value;

    String status;

    Boolean seen_by_bidder;

    Boolean seen_by_owner;
    
    @Basic(optional = false)
    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @OrderBy("created ASC")
    Date created;

    public Bid() {}

    public Bid(Integer id, String status, User user, Art art, Boolean seen_by_bidder, Boolean seen_by_owner, Integer value){
        this.id = id;
        this.status = status;
        this.user = user;
        this.art = art;
        this.seen_by_bidder=seen_by_bidder;
        this.seen_by_owner=seen_by_owner;
        this.value=value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Art getArt() {
        return art;
    }

    public void setArt(Art art) {
        this.art = art;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getSeen_by_bidder() {
        return seen_by_bidder;
    }

    public void setSeen_by_bidder(Boolean seen_by_bidder) {
        this.seen_by_bidder= seen_by_bidder;
    }

    public Boolean getSeen_by_owner() {
        return seen_by_owner;
    }

    public void setSeen_by_owner(Boolean seen_by_owner) {
        this.seen_by_owner= seen_by_owner;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
