package pt.unl.fct.iadi.main.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pt.unl.fct.iadi.main.model.User;

import org.springframework.stereotype.Component;

@Component
public class JwtGenerator {

    public String generate(User user) {

    	Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("id", user.getId() + "");
        claims.put("username", user.getUsername());
   
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.getSecret())
                .compact();
    }
}
