package pt.unl.fct.iadi.main.controllers;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RequestParam;
        import org.springframework.web.bind.annotation.RestController;

        import pt.unl.fct.iadi.main.model.Content;
        import pt.unl.fct.iadi.main.model.ContentRepository;


// Inspired in: https://spring.io/guides/gs/rest-service/

@RestController
@RequestMapping(value="/content")
public class ContentController {

    @Autowired
    ContentRepository content;

    @RequestMapping(value="", method= RequestMethod.GET)
    Content getAll(@RequestParam(required=false, value="") String search) {
        return content.findById(1);
    }
}
