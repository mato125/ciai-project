package pt.unl.fct.iadi.main.dto;

import java.util.List;
import java.util.Map;

public class UserForLogin {

    String username;
    String password;
    
    List<String> keywords;
    
    Map<String, String> obj;
    
    public List<String> getKeywords() {
		return keywords;
	}




	public Map<String, String> getObj() {
		return obj;
	}




	public void setObj(Map<String, String> obj) {
		this.obj = obj;
	}




	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}




	public UserForLogin() {};

    
   
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return 	
        		username + ", " + 
        		password;
    }
}
