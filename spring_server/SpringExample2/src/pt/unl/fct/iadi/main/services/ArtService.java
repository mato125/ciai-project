package pt.unl.fct.iadi.main.services;


import pt.unl.fct.iadi.main.dto.ArtToBeAdded;
import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.model.User;

public interface ArtService {
		
	public void addNewArt(ArtToBeAdded newArt, User loggedUser);
	
}
