package pt.unl.fct.iadi.main.model;

        import com.fasterxml.jackson.annotation.JsonManagedReference;
        import org.hibernate.annotations.Table;

        import javax.persistence.*;
        import java.util.Date;
import java.util.List;
import java.util.Set;

// Basic JPA configuration: https://spring.io/guides/gs/accessing-data-jpa/
// For mysql configuration: https://spring.io/guides/gs/accessing-data-mysql/

@Entity(name = "art")
public class Art {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="art_id_seq")
	@SequenceGenerator(name="art_id_seq", sequenceName="art_id_seq", allocationSize=1)
	@Column(name = "id")
    int id;

    String name;
    
    @ManyToOne
    @JoinColumn(name = "author", nullable = false)
    User author;

    @ManyToOne
    @JoinColumn(name = "owner", nullable = false)
    User owner;

    String description;

    Boolean available;

    Boolean publicOwner;

    String technique;

    Integer price;
    
    @Basic(optional = false)
    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @OrderBy("created ASC")
    Date created;

    @OneToMany(mappedBy="art", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Content> content;    

    @OneToMany(mappedBy="art", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Bid> bids;
    
    @ManyToMany
    @JsonManagedReference
    @JoinTable(
        name="art_keywords_map",
        joinColumns=@JoinColumn(name="art_id", referencedColumnName="id"),
        inverseJoinColumns=@JoinColumn(name="keyword_id", referencedColumnName="id"))
    private List<Keyword> keywords;
    
    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }


    

    public List<Keyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<Keyword> keywords) {
		this.keywords = keywords;
	}

	public List<Bid> getBids() {
		return bids;
	}

	public List<Bid> getBid() {
        return bids;
    }

    public void setBids(List<Bid> bids) {
        this.bids = bids;
    }


    public Art() {}

    public Art(Integer id, String name, User author, User owner, String description, Boolean available, Boolean publicOwner, String technique, Integer price, Date created) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.owner = owner;
        this.description=description;
        this.available=available;
        this.publicOwner=publicOwner;
        this.technique = technique;
        this.price=price;
        this.created=created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean getPublicOwner() {
        return publicOwner;
    }

    public void setPublicOwner(Boolean publicOwner) {
        this.publicOwner = publicOwner;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getTechnique() {
        return technique;
    }

    public void galleryData(){
        this.author.setPassword("");
        this.price=0;
        this.owner= null;
    }

    public void setTechnique(String technique) {
        this.technique = technique;
    }
    @Override
    public String toString() {
        String result = String.format(
                "Art[id=%d, name='%s', technique='%s', author='%d']%n",
                id, name, technique, author);
        if (content != null) {
            for(Content c : content) {
                result += String.format(
                        "Content[id=%d, type='%s', url='%s']%n",
                        c.getId(), c.getType(), c.getUrl());
            }
        }

        return result;
    }

    /*
    public String toString() {
        return id + ":" + name +":" +  author +  ", " + technique ;
    }
    */
}
