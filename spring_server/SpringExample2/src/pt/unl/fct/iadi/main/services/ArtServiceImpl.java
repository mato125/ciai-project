package pt.unl.fct.iadi.main.services;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.unl.fct.iadi.main.dto.ArtToBeAdded;
import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.exceptions.BadRequestException;
import pt.unl.fct.iadi.main.exceptions.NewArtValidaitonFailedException;
import pt.unl.fct.iadi.main.exceptions.RegistrationValidaitonFailedException;
import pt.unl.fct.iadi.main.model.Art;
import pt.unl.fct.iadi.main.model.ArtBuilder;
import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.model.Content;
import pt.unl.fct.iadi.main.model.ContentBuilder;
import pt.unl.fct.iadi.main.model.ContentRepository;
import pt.unl.fct.iadi.main.model.Keyword;
import pt.unl.fct.iadi.main.model.KeywordBuilder;
import pt.unl.fct.iadi.main.model.KeywordRepository;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserRepository;
import pt.unl.fct.iadi.main.validations.ArtValidations;
import pt.unl.fct.iadi.main.validations.UserValidations;

@Service
public class ArtServiceImpl implements ArtService {
	
	 @Autowired
	 ArtRepository artRepo;
	 
	 @Autowired
	 ContentRepository contentRepo;
	 
	 @Autowired
	 UserRepository userRepo;
	 
	 @Autowired
	 KeywordRepository keywordRepo;
	 
	 @Autowired
	 ArtValidations artValidations;
	 
	@Override
	public void addNewArt(ArtToBeAdded newArtData, User loggedUser) {
		
		User author = userRepo.findById(loggedUser.getId());
		
		ArtValidations result = artValidations.validateNewArt(newArtData, author);
		if(result.isValid()) {
			
			DateFormat format = new SimpleDateFormat("YYY/MM/DD", Locale.ENGLISH);

			
			
			ArtBuilder newArt = new ArtBuilder()
					.name(newArtData.getName())
					.author(author)
					.owner(author)
					.available(newArtData.isAvailable())
					.technique(newArtData.getTechniques())
					.description(newArtData.getDescription())
					.price(newArtData.getPrice());
			
			List<Content> content = this.makeContentObjects(newArtData, newArt.build());
			List<Keyword> keywords = this.makeKeywordObjects(newArtData, newArt.build());
			
			newArt.content(content);
			newArt.keywords(keywords);
			artRepo.save(newArt.build());
			
		}else {
			throw new NewArtValidaitonFailedException(result.getErrorContainer());
		}

	}
	
	List<Content> makeContentObjects(ArtToBeAdded newArtData, Art art) {
		List<Content> contentListCreated = new ArrayList<Content>();
		
		for(HashMap<String, String> content : newArtData.getContent()) {
			ContentBuilder contentBuilder = new ContentBuilder()
			.art(art)
			.type("img")
			.url(content.get("text"));
				
			contentRepo.save(contentBuilder.build());
			contentListCreated.add(contentBuilder.build());
		}
		return contentListCreated;
		
	}
	
	List<Keyword> makeKeywordObjects(ArtToBeAdded newArtData, Art art) {
		List<Keyword> keywordListCreated = new ArrayList<Keyword>();
		List<Art> arts = new ArrayList<Art>();
		arts.add(art);
		for(HashMap<String, String> keyword : newArtData.getTags()) {
			
			KeywordBuilder keywordBuilder = new KeywordBuilder()
			.arts(arts)
			.word(keyword.get("text"));
			
			Keyword alreadyExists = keywordRepo.findByWord(keyword.get("text"));
			if(alreadyExists == null) {
				keywordRepo.save(keywordBuilder.build());
				keywordListCreated.add(keywordBuilder.build());
			}else {
				keywordListCreated.add(alreadyExists);
			}
			
			
		}
		return keywordListCreated;
		
	}

}
