
package pt.unl.fct.iadi.main.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pt.unl.fct.iadi.main.validations.ValidationErrorContainer;


@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NewArtValidaitonFailedException extends RuntimeException {
    
	ValidationErrorContainer errors;
	public NewArtValidaitonFailedException(ValidationErrorContainer errors){
		this.errors = errors;
	}
	
	public ValidationErrorContainer getErrors() {
		return this.errors;
	}
}
