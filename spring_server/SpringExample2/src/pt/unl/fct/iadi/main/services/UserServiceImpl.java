package pt.unl.fct.iadi.main.services;


import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.unl.fct.iadi.main.exceptions.RegistrationValidaitonFailedException;
import pt.unl.fct.iadi.main.exceptions.ResourceNotFoundException;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserBuilder;
import pt.unl.fct.iadi.main.model.UserRepository;
import pt.unl.fct.iadi.main.security.JwtGenerator;
import pt.unl.fct.iadi.main.dto.UserForRegistration;
import pt.unl.fct.iadi.main.validations.UserValidations;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository user;
	 
	@Autowired
	UserValidations userValdiations;
	 
	public UserServiceImpl(){}
	
	@Override
	public String loginUser(String username, String password) throws ResourceNotFoundException{
		
		User userFromDatabase = user.findByUsername(username);
				
		if(userFromDatabase != null) {
			
			if(BCrypt.checkpw(password, userFromDatabase.getPassword())){
				
				return new JwtGenerator().generate(userFromDatabase);
				
			}else {
				throw new ResourceNotFoundException();
			}
			
		}else {
			throw new ResourceNotFoundException();
		}
	}
	
	@Override
	public	void registerNewUser(UserForRegistration newUserData) throws RegistrationValidaitonFailedException{
	
		UserValidations result = userValdiations.validateNewUser(newUserData);
		if(result.isValid()) {
			newUserData.setHashedSaltedPassword(BCrypt.hashpw(newUserData.getPassword(), BCrypt.gensalt()));
			UserBuilder newUser = new UserBuilder()
				.id(newUserData.getId())
				.firstName(newUserData.getFirstName())
				.lastName(newUserData.getLastName())
				.username(newUserData.getUsername())
				.password(newUserData.getHashedSaltedPassword())
				.imgUrl(newUserData.getImgUrl())
				.description(newUserData.getDescription());
			
			user.save(newUser.getUser());

			
		}else {
			throw new RegistrationValidaitonFailedException(result.getErrorContainer());
		}

	}
	

}
