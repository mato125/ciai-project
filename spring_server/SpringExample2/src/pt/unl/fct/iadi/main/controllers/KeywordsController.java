package pt.unl.fct.iadi.main.controllers;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pt.unl.fct.iadi.main.model.*;
import pt.unl.fct.iadi.main.exceptions.ResourceNotFoundException;

@RestController
@RequestMapping(value="/keywords")
public class KeywordsController {

    @Autowired
    KeywordRepository keywords;


    @RequestMapping(value="", method= RequestMethod.GET)
    ResponseEntity getAll(@RequestParam(required=false, value="") String search) {

        Keyword keyword = keywords.findById(2);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(keyword);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    ResponseEntity  showArt(@PathVariable Integer id) {

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("foo");

    }
}
