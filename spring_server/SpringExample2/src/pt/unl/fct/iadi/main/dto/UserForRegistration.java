package pt.unl.fct.iadi.main.dto;

public class UserForRegistration {

    int id;

    String username;
    String firstName;
	String lastName;
    String password;
    String passwordConf;
    String description;
    String imgUrl;
    String hashedSaltedPassword;
    
    public UserForRegistration() {};

    
    public String getPasswordConf() {
		return passwordConf;
	}


	public void setPasswordConf(String passwordConf) {
		this.passwordConf = passwordConf;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getImgUrl() {
		return imgUrl;
	}


	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
        

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        return 	id + "," + 
        		username + ", " + 
        		password + ", " +
        		passwordConf + ", " +
        		firstName + ", " + 
        		lastName + ", " +
        		description + ", " + 
        		imgUrl;
    }

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	 public String getHashedSaltedPassword() {
		return hashedSaltedPassword;
	}

	public void setHashedSaltedPassword(String hashedSaltedPassword) {
		this.hashedSaltedPassword = hashedSaltedPassword;
	}
}
