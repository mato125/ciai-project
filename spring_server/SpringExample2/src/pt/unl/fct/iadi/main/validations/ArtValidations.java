package pt.unl.fct.iadi.main.validations;

import java.util.HashMap;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.unl.fct.iadi.main.dto.ArtToBeAdded;
import pt.unl.fct.iadi.main.model.Art;
import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.model.User;

@Service
public class ArtValidations {
	
	@Autowired
	ArtRepository artRepo;
	
	private boolean isValid;
	
	public ArtValidations() {	};
	
	private ValidationErrorContainer errorContainer = new ValidationErrorContainer();;
	
	public boolean isValid() {
		return this.isValid;
	}
	
	public ValidationErrorContainer getErrorContainer() {
		return this.errorContainer;
	}
	
	public ArtValidations validateNewArt(ArtToBeAdded newArtData, User loggedUser) {
		this.errorContainer = new ValidationErrorContainer();
		
		this.validateNewArtForm(newArtData, loggedUser);
		this.validateNewArtDatabase(newArtData, loggedUser);
		
		if(errorContainer.getErrors().size() == 0) {
			this.isValid = true;
		}
		
		return this;
	}
	
	public void validateNewArtDatabase(ArtToBeAdded newArtData, User loggedUser) {	
		if(loggedUser != null) {

			Art art = artRepo.findByName(newArtData.getName());
			
			if(art != null) {
				errorContainer.put("name", "There is an art with such name");
			}
		}else {
			errorContainer.put("author", "Author or owner is invalid");
		}
	}
	
	public void validateNewArtForm(ArtToBeAdded newArtData, User loggedUser) {
				
		final String fieldRequired = "This field is required";
		final String filedIsNotUrl = "Please set valid url address";
		
		if(newArtData.getName() == null || newArtData.getName().length() == 0){
			errorContainer.put("name", fieldRequired);
		}
		
		if(newArtData.getDescription() == null || newArtData.getDescription().length() == 0){
			errorContainer.put("description", fieldRequired);
		}
		

		if(newArtData.getTechniques() == null || newArtData.getTechniques().length() == 0){
			errorContainer.put("techniques", fieldRequired);
		}
		
		if(newArtData.getTags() == null || newArtData.getTags().size() == 0){
			errorContainer.put("tags", fieldRequired);
		}
		
		if(newArtData.getContent() == null || newArtData.getContent().size() == 0){
			errorContainer.put("tags", fieldRequired);
		}else {
			UrlValidator urlValidator = new UrlValidator();
			for(HashMap<String, String> content : newArtData.getContent()) {
				if(content.get("text") == null || !urlValidator.isValid(content.get("text")) ) {
					errorContainer.put("tags", filedIsNotUrl);
				}
			}
		}

	}
}
