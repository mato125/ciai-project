package pt.unl.fct.iadi.main.model;

import org.springframework.data.repository.CrudRepository;

public interface KeywordRepository extends CrudRepository<Keyword,Integer> {
    Keyword findById(Integer id);

	Keyword findByWord(String string);
}
