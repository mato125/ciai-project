package pt.unl.fct.iadi.main.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.unl.fct.iadi.main.dto.JwtUserDetails;
import pt.unl.fct.iadi.main.model.ArtRepository;
import pt.unl.fct.iadi.main.model.Bid;
import pt.unl.fct.iadi.main.model.BidRepository;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserRepository;

@Service("securityService")
public class SecurityService {
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	BidRepository bidRepo;
	
	@Autowired
	ArtRepository artRepo;
	
	public boolean canModifyArt(JwtUserDetails user, int artId) {
		
		return artRepo.artBelongsToUser(artId, user.getId()) != null;
		
	}
	
	public boolean canModifyBid(JwtUserDetails user, int bidId) {
		Bid bid = bidRepo.findById(bidId);
		if(bid != null) {
			return bid.getArt().getAuthor().getId() == user.getId();
		}
		return false;		
	}
	
	
	public boolean canPublishBid(JwtUserDetails user, int bidId) {
		Bid bid = bidRepo.findById(bidId);
		if(bid != null) {
			return bid.getUser().getId() == user.getId() && bid.getStatus().equals("accepted");
		}
		return false;		
	}
}

