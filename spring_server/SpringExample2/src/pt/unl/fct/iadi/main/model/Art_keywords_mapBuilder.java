package pt.unl.fct.iadi.main.model;

public class Art_keywords_mapBuilder {

    private Art_keywords_map art_keywords_map;

    public Art_keywords_mapBuilder() {
        art_keywords_map = new Art_keywords_map();
    }

    public Art_keywords_mapBuilder id(int id) {
        art_keywords_map.setId(id);
        return this;
    }


    public Art_keywords_mapBuilder art(Art art) {
        art_keywords_map.setArt(art);
        return this;
    }


    public Art_keywords_map build() {
        return art_keywords_map;
    }
   
}


