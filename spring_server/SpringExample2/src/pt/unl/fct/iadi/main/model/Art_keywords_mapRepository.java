package pt.unl.fct.iadi.main.model;

import org.springframework.data.repository.CrudRepository;

public interface Art_keywords_mapRepository extends CrudRepository<Art_keywords_map,Integer> {

    Art_keywords_map findById(Integer id);
}
