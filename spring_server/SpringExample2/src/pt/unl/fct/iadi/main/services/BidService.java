package pt.unl.fct.iadi.main.services;

import pt.unl.fct.iadi.main.dto.NewBidData;
import pt.unl.fct.iadi.main.model.User;

public interface BidService {
	
	public void makeNewBid(User user, NewBidData bidData);
}
