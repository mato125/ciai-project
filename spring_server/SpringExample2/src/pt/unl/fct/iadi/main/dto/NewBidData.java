package pt.unl.fct.iadi.main.dto;

public class NewBidData {
	
	int value;
	int artId;
	
	public NewBidData() {}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getArtId() {
		return artId;
	}

	public void setArtId(int artId) {
		this.artId = artId;
	}
	
	
}
