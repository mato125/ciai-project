package pt.unl.fct.iadi.main.model;

public class UserBuilder {

    private User user;

    public UserBuilder() {
    	user = new User();
    }

    public UserBuilder id(int id) {
    	user.setId(id);
        return this;
    }

    public UserBuilder username(String username) {
    	user.setUsername(username);
        return this;
    }

    public UserBuilder firstName(String firstName) {
    	user.setFirstName(firstName);
        return this;
    }
    
    public UserBuilder lastName(String lastName) {
    	user.setLastName(lastName);
        return this;
    }
    
    public UserBuilder password(String password) {
    	user.setPassword(password);
        return this;
    }
    
    public UserBuilder description(String description) {
    	user.setDescription(description);
        return this;
    }
    
    public UserBuilder imgUrl(String imgUrl) {
    	user.setImgUrl(imgUrl);
        return this;
    }
    
    public User getUser() {
    	return this.user;
    }

}
