package pt.unl.fct.iadi.main.model;

        import java.util.List;

import org.springframework.data.jpa.repository.Query;
        import org.springframework.data.repository.CrudRepository;


public interface BidRepository extends CrudRepository<Bid,Integer> {
    Bid findById(Integer id);
    
    @Query(value = "SELECT b.*, u.* FROM bid AS b JOIN users AS u ON u.id = b.user_id" ,nativeQuery = true)
	Bid[] findAllWithUsers();
    
    @Query(value = "SELECT * FROM bid AS b where art_id = ?1 ORDER BY created DESC LIMIT 1" ,nativeQuery = true)
	Bid getLastBidOfArt(int id);
    
    @Query(value = "SELECT * FROM bid AS b where art_id = ?1" ,nativeQuery = true)
    List<Bid> findByArtId(int artId);

}
