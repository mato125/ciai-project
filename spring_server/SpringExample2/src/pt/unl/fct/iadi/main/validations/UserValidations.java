package pt.unl.fct.iadi.main.validations;


import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.unl.fct.iadi.main.dto.UserForRegistration;
import pt.unl.fct.iadi.main.model.User;
import pt.unl.fct.iadi.main.model.UserRepository;


@Service
public class UserValidations {
	
	
	@Autowired
	UserRepository user;
	
	private boolean isValid;
	
	public UserValidations() {	};
	
	private ValidationErrorContainer errorContainer = new ValidationErrorContainer();;
	
	public boolean isValid() {
		return this.isValid;
	}
	
	public ValidationErrorContainer getErrorContainer() {
		return this.errorContainer;
	}
	
	public UserValidations validateNewUser(UserForRegistration newUser) {
		this.errorContainer = new ValidationErrorContainer();
		
		this.validateNewUserForm(newUser);
		this.validateNewUserDatabase(newUser);
		
		if(errorContainer.getErrors().size() == 0) {
			this.isValid = true;
		}
		
		return this;
	}
	
	public void validateNewUserDatabase(UserForRegistration newUser) {
		User userFromDb = user.findByUsername(newUser.getUsername());
		if(userFromDb != null) errorContainer.put("username", "There is user with such name");
	}
	
	public void validateNewUserForm(UserForRegistration newUser) {
				
		final String fieldRequired = "This field is required";
		final String filedIsNotUrl = "Please set valid url address";
		
		if(newUser.getUsername() == null || newUser.getUsername().length() == 0){
			errorContainer.put("username", fieldRequired);
		}
		
		if(newUser.getFirstName() == null || newUser.getFirstName().length() == 0){
			errorContainer.put("firstName", fieldRequired);
		}
		
		if(newUser.getLastName() == null || newUser.getLastName().length() == 0){
			errorContainer.put("lastName", fieldRequired);
		}

		if(newUser.getPassword() == null || newUser.getPassword().length() == 0){
			errorContainer.put("password", fieldRequired);
		}else {
			if(!newUser.getPasswordConf().equals(newUser.getPassword())){
				errorContainer.put("passwordConf", "Password must match");
			}
		}
		
		if(newUser.getPasswordConf() == null || newUser.getPasswordConf().length() == 0){
			errorContainer.put("passwordConf", fieldRequired);
		}else {
			if(!newUser.getPasswordConf().equals(newUser.getPassword())){
				errorContainer.put("passwordConf", "Password must match");
			}
		}
		
		if(newUser.getDescription() == null || newUser.getDescription().length() == 0){
			errorContainer.put("description", fieldRequired);
		}
				  
		if(newUser.getImgUrl() == null || newUser.getImgUrl().length() == 0){
			errorContainer.put("imgUrl", fieldRequired);
		}else {
			UrlValidator urlValidator = new UrlValidator();
			if(!urlValidator.isValid(newUser.getImgUrl())){
				errorContainer.put("imgUrl", filedIsNotUrl);
			}
		}
	}

}
