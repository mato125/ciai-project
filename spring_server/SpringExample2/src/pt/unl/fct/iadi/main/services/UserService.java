package pt.unl.fct.iadi.main.services;

import pt.unl.fct.iadi.main.dto.UserForRegistration;

public interface UserService {
	
	public String loginUser(String username, String password);

	public void registerNewUser(UserForRegistration newUser);
}
