import path from 'path'
import webpack from 'webpack'
import ExtractTextPlugin from "extract-text-webpack-plugin"

export default{
	entry: [
		'webpack-hot-middleware/client',
		path.join(__dirname, '/client/index.js')
	],
	output: {
		path: '/',
		publicPath: '/'
	},
	plugins: [
		new webpack.NoErrorsPlugin(),
		new webpack.optimize.OccurenceOrderPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new ExtractTextPlugin({
			filename: "style.css",
			disable: process.env.NODE_ENV === "development"
		})
	],
	module:{
		loaders: [
			{
				test: /\.js$/,
				include: [
					path.join(__dirname, 'client'),
					path.join(__dirname, 'server/shared')
					],
				loaders: ['react-hot', 'babel']
			},
			{
				test: /\.less$/,
				include: [
					path.join(__dirname, 'client/css'),
				],
				loaders: ['style-loader', 'css-loader', 'less-loader']
			}
		]
	},
	resolve:{
		extentions: ['.js']
	},
	node: {
		net: 'empty',
		dns: 'empty'
	}
}