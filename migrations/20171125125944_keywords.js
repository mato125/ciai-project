exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTableIfNotExists("keywords", function (table) {
            table.increments();
            table.string('word').notNullable().unique();
        }).then(function () {
                return knex("keywords").insert([
                    {word: 'Sculpture'},
                    {word: 'Painting'},
                    {word: 'Music'},
                    {word: 'Sadness'},
                ]);
            }
        ),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('keywords');
};

