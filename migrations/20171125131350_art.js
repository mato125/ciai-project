exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTableIfNotExists("art", function (table) {
            table.increments();
            table.string('name').notNullable().unique();
            table.integer('author').notNullable().index().references('id').inTable('users');
            table.integer('owner').notNullable().index().references('id').inTable('users');
            table.string('description');
            table.string('technique');
            table.date('created');
            table.boolean('public_owner');
            table.boolean('available');
            table.string('price');
            table.timestamps();
        }).then(function () {
                return knex("art").insert([
                    {
                        name: 'Lorem ipsum',
                        author: 1,
                        owner: 1,
                        description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis.",
                        technique: "Fast painting",
                        created: "2017-04-27 22:06:17.355+02",
                        public_owner: true,
                        available: true,
                        price: 2200
                    }
                ]);
            }
        ),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('art');
};

