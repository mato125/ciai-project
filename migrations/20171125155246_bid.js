exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTableIfNotExists("bid", function (table) {
            table.increments();
            table.integer('art_id').notNullable().index().references('id').inTable('art');
            table.integer('user_id').notNullable().index().references('id').inTable('users');
            table.integer('value').notNullable();
            table.string('status').notNullable();
            table.boolean('seen_by_bidder').notNullable();
            table.boolean('seen_by_owner').notNullable();
        }).then(function () {
                return knex("bid").insert([
                    {
                        art_id: 1,
                        user_id: 2,
                        value: 7500,
                        status: "new",
                        seen_by_bidder: false,
                        seen_by_owner: false
                    },
                    {
                        art_id: 1,
                        user_id: 3,
                        value: 8500,
                        status: "new",
                        seen_by_bidder: false,
                        seen_by_owner: false
                    }
                ]);
            }
        ),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('content');
};

