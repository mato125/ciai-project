exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTableIfNotExists("art_keywords_map", function (table) {
            table.increments();
            table.integer('art_id').notNullable().index().references('id').inTable('art');
            table.integer('keyword_id').notNullable().index().references('id').inTable('keywords');
        }).then(function () {
                return knex("art_keywords_map").insert([
                    {
                        art_id: 1,
                        keyword_id: 2,
                    },
                    {
                        art_id: 1,
                        keyword_id: 1,
                    },
                    {
                        art_id: 1,
                        keyword_id: 3,
                    }
                ]);
            }
        ),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('art_keywords_map');
};

