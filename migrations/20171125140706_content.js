exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTableIfNotExists("content", function (table) {
            table.increments();
            table.integer('art_id').notNullable().index().references('id').inTable('art');
            table.string('url').notNullable();
            table.string('type').notNullable();
        }).then(function () {
                return knex("content").insert([
                    {
                        art_id: 1,
                        url: "https://thumb9.shutterstock.com/display_pic_with_logo/510316/583771252/stock-photo-the-poster-with-a-portrait-of-a-beautiful-girl-in-the-style-of-contemporary-art-583771252.jpg",
                        type: "img"
                    },
                    {
                        art_id: 1,
                        url: "https://thumb7.shutterstock.com/display_pic_with_logo/228151/459092947/stock-photo-abstract-art-creative-background-hand-painted-background-459092947.jpg",
                        type: "img"
                    }
                ]);
            }
        ),
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('content');
};

