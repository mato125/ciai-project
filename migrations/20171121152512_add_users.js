exports.up = function (knex, Promise) {
    return Promise.all([
                 knex("users").insert([
                    {username: 'Bmojsej',
                        hashed_salted_password: '$2a$10$G6v1BrPqO1raxrtOI/PGpOsZ8W2i/KBmkydvozBTDg8Kg89CwiR/y',
                        first_name: "Brano",
                        last_name: "Mojsej",
                        img_url: "https://thumb1.shutterstock.com/display_pic_with_logo/2117717/445337599/stock-photo-artist-creative-designer-illustrator-graphic-skill-concept-445337599.jpg",
                        description:"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean et est a dui semper facilisis. "
                    },
                    {username: 'Jprocko',
                        hashed_salted_password: '$2a$10$G6v1BrPqO1raxrtOI/PGpOsZ8W2i/KBmkydvozBTDg8Kg89CwiR/y',
                        first_name: "Jozo",
                        last_name: "Procko",
                        img_url: "https://thumb9.shutterstock.com/display_pic_with_logo/334153/487267645/stock-photo-the-artist-draws-graffiti-487267645.jpg",
                        description:"Pellentesque placerat elit a nunc. Nullam tortor odio, rutrum quis, egestas ut, posuere sed, felis."
                    },
                    {username: 'RoboF',
                        hashed_salted_password: '$2a$10$G6v1BrPqO1raxrtOI/PGpOsZ8W2i/KBmkydvozBTDg8Kg89CwiR/y',
                        first_name: "Robo",
                        last_name: "Fico",
                        img_url: "https://t4.aimg.sk/magaziny/DGQZrByiQzWknIuRwdHYWA~Robo-fico-sexy-robert-slovak.jpg?t=LzEyMDB4NjMw&h=qrRS7NJ88trUh52-kZy8zQ&e=2145916800&v=2",
                        description:"Wrum quis, egestas ut, posuere sed, felis. Vestibulum placerat feugiat nisl."
                    }
                ])
    ]);
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users');
};

