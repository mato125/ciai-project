
exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', function(table){
        table.increments();
        table.string('username').notNullable().unique();
        table.string('first_name').notNullable();
        table.string('last_name').notNullable();
        table.string('description').notNullable();
        table.string('img_url').notNullable();
        table.string('hashed_salted_password').notNullable();
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users');
};
